![Draw Gaiden](https://gitlab.com/cschram/drawgaiden/raw/master/packages/client/src/img/logo_big.png)

Draw Gaiden is a collaborative drawing web application for drawing on a shared canvas with friends.

# Features

* Public links for sharing
* Real-time collaboration
* Pencil, Rectangle, Circle, Eraser, and Color Picker tools
* Displays the location of users cursors while they are drawing
* Background service to clean up unused canvases and squash canvas history into snapshots
* Save canvas snapshot images

Installation and other documentation can be found on the [wiki](https://gitlab.com/cschram/drawgaiden/wikis/home).

# License

Draw Gaiden is made available under the terms of the [MIT License](https://en.wikipedia.org/wiki/MIT_License).