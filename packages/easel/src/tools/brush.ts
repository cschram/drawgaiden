import { Tool } from "./tool";
import {
    ToolSettings,
    Coord,
    Layer,
    getDistance,
    getAngle
} from "../util";
import * as simplify from "simplify-js";

const TOLERANCE = 0.8;
const BRUSH_TEXTURE_SIZE = 100;

export default class BrushTool extends Tool {
    static brushes: { [name: string]: HTMLImageElement } = {};

    protected cache: { [key: string]: HTMLImageElement };

    constructor(layers: Layer[], settings: ToolSettings = {}) {
        super(layers, settings);
        this.cache = {};
    }

    protected getImage(name: string, color: string): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            if (!BrushTool.brushes.hasOwnProperty(name)) {
                reject(`Brush "${name}" does not exist.`);
            } else {
                const key = `${name}:${color}`;
                if (this.cache.hasOwnProperty(name)) {
                    resolve(this.cache[key]);
                } else {
                    const canvas = document.createElement("canvas");
                    canvas.width = BRUSH_TEXTURE_SIZE;
                    canvas.height = BRUSH_TEXTURE_SIZE;
                    const context = canvas.getContext("2d") as CanvasRenderingContext2D;
                    context.drawImage(BrushTool.brushes[name], 0, 0);
                    context.fillStyle = color;
                    context.globalCompositeOperation = "source-in";
                    context.fillRect(0, 0, BRUSH_TEXTURE_SIZE, BRUSH_TEXTURE_SIZE);
                    const image = new Image();
                    image.src = canvas.toDataURL("image/png");
                    image.onload = () => {
                        this.cache[key] = image;
                        resolve(image);
                    };
                    image.onerror = reject;
                }
            }
        });
    }

    protected async drawLine(start: Coord, end: Coord, context: CanvasRenderingContext2D, settings: ToolSettings) {
        if (settings.brush) {
            const image = await this.getImage(settings.brush, settings.strokeStyle as string);
            const distance = getDistance(start, end);
            const angle = getAngle(start, end);
            for (let i = 0; i < distance; i++) {
                context.drawImage(
                    image,
                    start.x + (Math.sin(angle) * i) - (settings.lineWidth as number / 2),
                    start.y + (Math.cos(angle) * i) - (settings.lineWidth as number / 2),
                    settings.lineWidth as number,
                    settings.lineWidth as number
                );
            }
        }
    }

    async mouseMove(coord: Coord) {
        if (this.active && this.settings.brush) {
            const layer = this.layers[this.settings.layer as number];
            this._resetCtx(layer.draftCtx, this.settings);
            await this.drawLine(this.lastCoord, coord, layer.draftCtx, this.settings);
        }
        await super.mouseMove(coord);
    }

    async mouseUp(): Promise<Coord[]> {
        this.path = simplify(this.path, TOLERANCE);
        return super.mouseUp();
    }

    async draw(path: Coord[], settings: ToolSettings) {
        if (path.length === 0) {
            return;
        }

        settings = Object.assign({}, this.settings, settings);
        const layer = this.layers[settings.layer as number];

        layer.finalCtx.beginPath();
        this._resetCtx(layer.finalCtx, settings);

        if (path.length === 1) {
            if (settings.brush) {
                layer.finalCtx.drawImage(
                    await this.getImage(settings.brush, settings.strokeStyle as string),
                    path[0].x - (settings.lineWidth as number / 2),
                    path[0].y - (settings.lineWidth as number / 2),
                    settings.lineWidth as number,
                    settings.lineWidth as number
                );
            }
        } else {
            for (let i = 1; i < path.length; i++) {
                this.drawLine(
                    path[i - 1],
                    path[i],
                    layer.finalCtx,
                    settings
                );
            }
        }
    }
}