export interface Coord {
    x: number;
    y: number;
}

export interface Rect {
    x: number;
    y: number;
    width: number;
    height: number;
}

export interface Layer {
    id: number;
    finalCanvas: HTMLCanvasElement;
    finalCtx: CanvasRenderingContext2D;
    draftCanvas: HTMLCanvasElement;
    draftCtx: CanvasRenderingContext2D;
}

export interface ToolSettings {
    layer?: number;
    strokeStyle?: string;
    fillStyle?: string;
    lineWidth?: number;
    lineCap?: string;
    lineJoin?: string;
    opacity?: number;
    smoothLines?: boolean;
    brush?: string;
    globalCompositeOperation?: string;
    primary?: boolean;
    sendUpdates?: boolean;
}

export function toHex(v: number): string {
    const hex = v.toString(16);
    return hex.length === 1 ? "0" + hex : hex;
}

export function rgbToHex(r: number, g: number, b: number): string {
    return "#" + toHex(r) + toHex(g) + toHex(b);
}

export function getDistance(a: Coord, b: Coord): number {
    return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
}

export function getAngle(a: Coord, b: Coord): number {
    return Math.atan2(b.x - a.x, b.y - a.y);
}

export function getRect(coords: Coord[]): Rect {
    const topLeft = { x: coords[0].x, y: coords[0].y };
    const bottomRight = { x: coords[0].x, y: coords[0].y };
    for (let i = 1; i < coords.length; i++) {
        topLeft.x = Math.min(topLeft.x, coords[i].x);
        topLeft.y = Math.min(topLeft.y, coords[i].y);
        bottomRight.x = Math.max(bottomRight.x, coords[i].x);
        bottomRight.y = Math.max(bottomRight.y, coords[i].y);
    }
    return {
        x: topLeft.x,
        y: topLeft.y,
        width: (bottomRight.x - topLeft.x),
        height: (bottomRight.y - topLeft.y)
    };
}