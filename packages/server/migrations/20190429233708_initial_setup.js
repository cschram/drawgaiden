exports.up = function(knex, Promise) {
    return knex.schema
        .createTable("users", (table) => {
            table.increments("id");
            table.string("sub").notNullable();
            table.unique("sub");
            table.string("username").notNullable();
            table.string("avatar").notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.timestamp("last_login").defaultTo(knex.fn.now());
        })
        .createTable("sessions", (table) => {
            table.increments("id");
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.string("auth_token").notNullable();
            table.unique("auth_token");
            table.timestamp("created_at").defaultTo(knex.fn.now());
        })
        .createTable("canvases", (table) => {
            table.increments("id");
            table.integer("owner_id").unsigned();
            table.foreign("owner_id").references("users.id");
            table.string("title").notNullable();
            table.string("slug").notNullable();
            table.unique("slug");
            table.boolean("is_public").defaultTo(false);
            table.integer("width").unsigned();
            table.integer("height").unsigned();
            table.integer("layers").unsigned();
            table.string("background_color", 7).defaultTo("#ffffff");
            table.timestamp("created_at").defaultTo(knex.fn.now());
        })
        .createTable("history", (table) => {
            table.increments("id");
            table.integer("canvas_id").unsigned();
            table.foreign("canvas_id").references("canvases.id");
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.timestamp("created_at").defaultTo(knex.fn.now());
            table.string("tool_name").notNullable();
            table.json("path").notNullable();
            table.json("settings").notNullable();
        })
        .createTable("snapshots", (table) => {
            table.increments("id");
            table.integer("canvas_id").unsigned();
            table.foreign("canvas_id").references("canvases.id");
            table.integer("layer").unsigned();
            table.binary("data").notNullable();
            table.timestamp("created_at").defaultTo(knex.fn.now());
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("users")
        .dropTable("sessions")
        .dropTable("canvases")
        .dropTable("history")
        .dropTable("snapshots");
};
