const knex = require("knex");

const db = knex({
    client: "pg",
    connection: {
        host: "localhost",
        port: 5432,
        user: "postgres",
        password: "drawgaiden",
        database: "drawgaiden"
    }
});

async function test() {
    const result = await db
        .insert({
            sub: "test",
            username: "test",
            avatar: "test"
        })
        .returning("id")
        .into("users");
    console.log(result);
}

test().catch((error) => {
    console.error(error);
}).then(async () => {
    await db.delete().from("users");
    process.exit();
});