"use strict";

const fs = require("fs");
const path = require("path");
const globby = require("globby");
const yazl = require("yazl");
const pkg = require("../package.json");
const name = `${pkg.name.replace("@", "").replace("/", "_")}.zip`;

(async () => {
    console.log(`Packaging ${name}...`);
    const zip = new yazl.ZipFile();
    zip.addFile(".env", ".env");
    zip.addFile("package.json", "package.json");
    zip.addFile("package-lock.json", "package-lock.json");
    zip.addFile("ecosystem.config.json", "ecosystem.config.json");
    zip.addFile("bin/startup", "bin/startup");
    zip.addFile("knexfile.js", "knexfile.js");
    const scripts = await globby("migrations/*");
    scripts.forEach(file => zip.addFile(file, file));
    const modules = await globby("node_modules/**/*");
    modules.forEach(file => zip.addFile(file, file));
    const dist = await globby("dist/**/*");
    dist.forEach(file => zip.addFile(file, file));
    const brushes = await globby("../client/src/img/brushes/*");
    brushes.forEach(file => zip.addFile(file, path.join("brushes", path.basename(file))));
    zip.outputStream
        .pipe(fs.createWriteStream(path.resolve(__dirname, `../${name}`)))
        .on("close", () => {
            console.log("Done.");
            process.exit();
        });
    zip.end();
})();
