import * as r from "rethinkdb";
import * as DrawGaiden from "@drawgaiden/common";
import * as slug from "slug";
import * as shortid from "shortid";
import * as knex from "knex";
import config from "./config";

export interface CanvasFilter {
    owner_id?: number;
    title?: string;
    is_public?: boolean;
}

// export interface UserFilter {
//     canvas_id?: string;
//     skip?: number;
//     limit?: number;
// }

/**
 * Database connection wrapper.
 * Abstracts any necessary database operations.
 */
export class Connection {
    private pool: knex;

    constructor() {
        this.pool = knex({
            client: "pg",
            connection: {
                host: config.postgres.host,
                port: config.postgres.port,
                user: config.postgres.username,
                password: config.postgres.password,
                database: config.postgres.database,
                ssl: config.postgres.ssl
            }
        });
    }

    //#region User operations

    /**
     * Retrieve user by ID.
     * @param id User ID.
     */
    async getUser(id: number) : Promise<DrawGaiden.UserRecord> {
        const [ user ] = await this.pool
            .select()
            .from("users")
            .where({ id });
        return user;
    }

    /**
     * Retrieve user by sub (auth0 ID)
     * @param sub Auth0 ID.
     */
    async getUserBySub(sub: string) : Promise<DrawGaiden.UserRecord> {
        const [ user ] = await this.pool
            .select()
            .from("users")
            .where({ sub });
        return user;
    }

    /**
     * Retrieve users currently collaborating on a canvas.
     * @param filter User filter options
     */
    // async getUsers(filter: UserFilter) : Promise<Array<DrawGaiden.User>> {
    // }

    /**
     * Return the number of users subscribed to a canvas.
     * @param canvas_id ID of the canvas to count users for.
     */
    // getUserCount(canvas_id: string) {
    // }

    /**
     * Creates a new user
     * @param data User object.
     */
    async createUser(data: DrawGaiden.UserData): Promise<DrawGaiden.UserRecord> {
        const [ user ] = await this.pool
            .insert(data)
            .returning(["id", "sub", "username", "avatar", "created_at", "last_login"])
            .into("users");
        return user;
    }

    /**
     * Updates a user
     * @param user User object.
     */
    async updateUser(user: DrawGaiden.UserRecord): Promise<void> {
        await this.pool
            .update(user)
            .into("users")
            .where({ id: user.id });
    }

    /**
     * Update the users last login.
     * @param user_id ID of the user.
     */
    async loginUser(user_id: number): Promise<void> {
        await this.pool
            .update({
                last_login: this.pool.fn.now()
            })
            .into("users")
            .where({ id: user_id });
    }

    /**
     * Reset all users temporary state (canvas_id, mousePosition)
     */
    // resetUsers() {
    // }

    //#endregion

    //#region Session operations

    /**
     * Get a session by auth token.
     * @param token Auth0 token.
     */
    async getSession(auth_token: string): Promise<DrawGaiden.SessionRecord> {
        const [ session ] = await this.pool
            .select()
            .from("sessions")
            .where({ auth_token });
        return session;
    }

    /**
     * Get the user associated with a session.
     * @param auth_token Auth0 token.
     */
    async getSessionUser(auth_token: string): Promise<DrawGaiden.UserRecord> {
        const [ user ] = await this.pool
            .select([
                "users.id",
                "users.created_at",
                "users.last_login",
                "users.sub",
                "users.username",
                "users.avatar"
            ])
            .innerJoin("users", "sessions.user_id", "users.id")
            .from("sessions")
            .where({ auth_token });
        return user;
    }

    /**
     * Create a session.
     * @param session Session object to store.
     */
    async createSession(data: DrawGaiden.SessionData): Promise<DrawGaiden.SessionRecord> {
        const [ session ] = await this.pool
            .insert(data)
            .returning([ "id", "user_id", "created_at" ])
            .into("sessions");
        return session;
    }

    /**
     * Delete a session.
     * @param auth_token Auth0 token.
     */
    async deleteSession(auth_token: string): Promise<void> {
        await this.pool
            .delete()
            .from("sessions")
            .where({ auth_token });
    }

    /**
     * Clear any existing user sessions.
     * @param userID ID of the user.
     */
    async clearUserSessions(user_id: number): Promise<void> {
        await this.pool
            .delete()
            .from("sessions")
            .where({ user_id });
    }

    //#endregion

    //#region Canvas operations

    /**
     * Get canvas details by id.
     * @param id ID of the canvas.
     */
    async getCanvas(id: number): Promise<DrawGaiden.CanvasRecord<Buffer>> {
        const records = await this.pool
            .columns({
                id: "canvases.id",
                owner_id: "canvases.owner_id",
                owner_username: "users.username",
                title: "canvases.title",
                slug: "canvases.slug",
                is_public: "canvases.is_public",
                width: "canvases.width",
                height: "canvases.height",
                layers: "canvases.layers",
                background_color: "canvases.background_color",
                created_at: "canvases.created_at",
                snapshot: "snapshots.data"
            })
            .select()
            .leftJoin("snapshots", "canvases.id", "snapshots.canvas_id")
            .innerJoin("users", "canvases.owner_id", "users.id")
            .from("canvases")
            .where("canvases.id", id)
            .orderBy("snapshots.layer", "asc");
        if (records.length) {
            const canvas: DrawGaiden.CanvasRecord<Buffer> = records.reduce((canvas: DrawGaiden.CanvasRecord<Buffer>, record: any) => {
                if (record.snapshot) {
                    canvas.snapshots.push(record.snapshot);
                }
                return canvas;
            }, {
                id: records[0].id,
                owner_id: records[0].owner_id,
                owner_username: records[0].owner_username,
                title: records[0].title,
                slug: records[0].slug,
                is_public: records[0].is_public,
                width: records[0].width,
                height: records[0].height,
                layers: records[0].layers,
                background_color: records[0].background_color,
                created_at: records[0].created_at,
                snapshots: []
            });
            return canvas;
        }
        return null;
    }

    /**
     * Get canvas details by slug.
     * @param slug Canvas URL slug.
     */
    async getCanvasBySlug(slug: string): Promise<DrawGaiden.CanvasRecord<Buffer>> {
        const records = await this.pool
            .columns({
                id: "canvases.id",
                owner_id: "canvases.owner_id",
                owner_username: "users.username",
                title: "canvases.title",
                slug: "canvases.slug",
                is_public: "canvases.is_public",
                width: "canvases.width",
                height: "canvases.height",
                layers: "canvases.layers",
                background_color: "canvases.background_color",
                created_at: "canvases.created_at",
                snapshot: "snapshots.data"
            })
            .select()
            .leftJoin("snapshots", "canvases.id", "snapshots.canvas_id")
            .innerJoin("users", "canvases.owner_id", "users.id")
            .from("canvases")
            .where("canvases.slug", slug)
            .orderBy("snapshots.layer", "asc");
        if (records.length) {
            const canvas: DrawGaiden.CanvasRecord<Buffer> = records.reduce((canvas: DrawGaiden.CanvasRecord<Buffer>, record: any) => {
                if (record.snapshot) {
                    canvas.snapshots.push(record.snapshot);
                }
                return canvas;
            }, {
                id: records[0].id,
                owner_id: records[0].owner_id,
                owner_username: records[0].owner_username,
                title: records[0].title,
                slug: records[0].slug,
                is_public: records[0].is_public,
                width: records[0].width,
                height: records[0].height,
                layers: records[0].layers,
                background_color: records[0].background_color,
                created_at: records[0].created_at,
                snapshots: []
            });
            return canvas;
        }
        return null;
    }

    /**
     * Get canvases by owner ID.
     * @param filter Filter params.
     */
    async getCanvases(filter: CanvasFilter, skip = 0, limit = 20): Promise<Array<DrawGaiden.CanvasRecord<Buffer>>> {
        const canvases = await this.pool
            .select()
            .from("canvases")
            .where(filter)
            .limit(limit)
            .offset(skip);
        return canvases;
    }

    /**
     * Get a count of a users canvases.
     * @param owner_id ID of the user.
     */
    async getOwnerCanvasCount(owner_id: number): Promise<number> {
        return this.pool
            .count("id")
            .from("canvases")
            .where({ owner_id });
    }

    /**
     * Retrieve a list of every canvas ID.
     */
    async getCanvasIds(): Promise<Array<number>> {
        const records: Array<{ id: number }> = await this.pool
            .select([ "id" ])
            .from("canvases");
        return records.map(record => record.id);
    }

    /**
     * Create a new canvas.
     * @param ownerID ID of the user creating the canvas.
     * @param id Unique name of the canvas (used as the ID).
     */
    async createCanvas(data: DrawGaiden.CanvasData): Promise<DrawGaiden.CanvasRecord<Buffer>> {
        const [ result ] = await this.pool
            .insert({
                ...data,
                slug: `${slug(data.title)}-${shortid()}`
            })
            .returning(["id"])
            .into("canvases");
        return this.getCanvas(result.id);
    }

    /**
     * Update canvas details.
     * @param canvas Updated canvas details. Must contain canvas ID.
     */
    async updateCanvas(canvas: DrawGaiden.CanvasRecord<Buffer>): Promise<void> {
        await this.pool
            .update(canvas)
            .into("canvases")
            .where({ id: canvas.id });
    }

    /**
     * Delete a canvas.
     * @param id ID of the canvas to delete.
     */
    async deleteCanvas(id: number): Promise<void> {
        await this.clearHistory(id);
        await this.pool
            .delete()
            .from("canvases")
            .where({ id });
    }

    //#endregion

    //#region History operations

    /**
     * Retrieve history for a canvas.
     * @param canvas_id ID of the canvas to retrieve history for.
     */
    async getHistory(canvas_id: number, offset = 0, limit = 20): Promise<Array<DrawGaiden.HistoryRecord>> {
        return this.pool
            .select()
            .from("history")
            .where({ canvas_id })
            .offset(offset)
            .limit(limit);
    }

    /**
     * Get the last history entry for a canvas.
     * @param canvas_id ID of the canvas to retrieve history entry for.
     */
    async getLastHistoryEntry(canvas_id: number): Promise<DrawGaiden.HistoryRecord> {
        const [ history ] = await this.pool
            .select()
            .from("history")
            .where({ canvas_id })
            .orderBy("created_at", "desc")
            .limit(1);
        return history;
    }

    /**
     * Retrieve history count for a canvas.
     * @param canvas_id ID of the canvas to retrieve history count for.
     */
    async getHistoryCount(canvas_id: number): Promise<number> {
        return this.pool
            .count("id")
            .from("canvases")
            .where({ canvas_id });
    }

    /**
     * Add a new history entry. Timestamp is created for the entry.
     * @param entry History entry details.
     */
    async addHistory(entry: DrawGaiden.HistoryData): Promise<DrawGaiden.HistoryRecord> {
        const [ history_entry ] = await this.pool
            .insert({
                ...entry,
                path: JSON.stringify(entry.path),
                settings: JSON.stringify(entry.settings),
            })
            .returning([
                "id",
                "created_at",
                "canvas_id",
                "user_id",
                "tool_name",
                "path",
                "settings"
            ])
            .into("history");
        return history_entry;
    }

    /**
     * Clear history for a canvas.
     * @param canvas_id ID of the canvas to clear history for.
     * @param limit Optional limit for the number of entries to clear.
     */
    async clearHistory(canvas_id: number, limit = 0): Promise<void> {
        if (limit) {
            await this.pool
                .delete()
                .from("history")
                .where({ canvas_id })
                .limit(limit);
        } else {
            await this.pool
                .delete()
                .from("history")
                .where({ canvas_id });
        }
    }
    //#endregion
}