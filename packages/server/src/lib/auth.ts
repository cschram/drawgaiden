import axios, { AxiosResponse } from "axios";
import { UserData, UserRecord } from "@drawgaiden/common";
import { Connection } from "./db";
import config from "./config";

interface Profile {
    sub: string;
    nickname: string;
    picture: string;
}

export async function auth(token: string, db: Connection): Promise<UserRecord> {
    const user = await db.getSessionUser(token);
    if (user) {
        return user;
    } else {
        const profile: AxiosResponse<Profile> = await axios.get(`https://${config.auth.domain}/userinfo`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });
        let user: UserRecord = await db.getUserBySub(profile.data.sub);
        if (user) {
            // Update last login if user already exists
            await db.loginUser(user.id);
        } else {
            // Create user in the database if it does not already exist
            const user_data: UserData = {
                sub: profile.data.sub,
                username: profile.data.nickname.replace(/\s/g, "").toLowerCase(),
                avatar: profile.data.picture
            };
            user = await db.createUser(user_data);
        }
        await db.clearUserSessions(user.id);
        await db.createSession({
            user_id: user.id,
            auth_token: token
        });
        return user;
    }
}