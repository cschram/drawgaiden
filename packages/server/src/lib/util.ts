export async function asyncForEach<T>(arr: T[], callback: (item: T, index: number) => Promise<void>) {
    for (let i = 0; i < arr.length; i++) {
        await callback(arr[i], i);
    }
}