require("dotenv").config();

export default {
    logDirectory: process.env.LOG_DIRECTORY,
    postgres: {
        host: process.env.POSTGRES_HOST,
        port: parseInt(process.env.POSTGRES_PORT, 10),
        username: process.env.POSTGRES_USERNAME,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_DATABASE,
        ssl: process.env.POSTGRES_SSL === "true" ? true : false
    },
    redis: {
        host: process.env.REDIS_HOST,
        port: parseInt(process.env.REDIS_PORT, 10)
    },
    server: {
        host: process.env.SERVER_HOST,
        port: parseInt(process.env.SERVER_PORT, 10)
    },
    auth: {
        domain: process.env.AUTH_DOMAIN
    },
    janitor: {
        jobInterval: 300000,
        historyThreshold: 50,
        monitorPort: 9100
    },
    cache: {
        thumbnailTTL: parseInt(process.env.THUMBNAIL_TTL, 10)
    },
};