import { resolve } from "path";
import { createCanvas, Image } from "canvas";
import { CanvasRecord, HistoryRecord } from "@drawgaiden/common";
import { Tool } from "@drawgaiden/easel/lib/tools/tool";
import BrushTool from "@drawgaiden/easel/lib/tools/brush";
import CircleTool from "@drawgaiden/easel/lib/tools/circle";
import ColorPickerTool from "@drawgaiden/easel/lib/tools/colorpicker";
import EraserTool from "@drawgaiden/easel/lib/tools/eraser";
import PencilTool from "@drawgaiden/easel/lib/tools/pencil";
import RectangleTool from "@drawgaiden/easel/lib/tools/rectangle";
import { Layer } from "@drawgaiden/easel/lib/util";
import { asyncForEach } from "./util";

const BRUSH_TEXTURE_WIDTH = 100;
const BRUSH_TEXTURE_HEIGHT = 100;

function createImage(src: string | Buffer): HTMLImageElement {
    const image = new Image();
    image.src = src;
    return <any>image;
}

export class NodeBrushTool extends BrushTool {
    static brushes: { [name: string]: HTMLImageElement } = {
        "airbrush": createImage(resolve(__dirname, "../../../client/src/img/brushes/airbrush.png")),
        "chalk": createImage(resolve(__dirname, "../../../client/src/img/brushes/chalk.png")),
        "charcoal": createImage(resolve(__dirname, "../../../client/src/img/brushes/charcoal.png")),
        "paintbrush_flat_rough": createImage(resolve(__dirname, "../../../client/src/img/brushes/paintbrush_flat_rough.png")),
        "paintbrush_flat_soft": createImage(resolve(__dirname, "../../../client/src/img/brushes/paintbrush_flat_soft.png")),
        "paintbrush_smooth": createImage(resolve(__dirname, "../../../client/src/img/brushes/paintbrush_smooth.png")),
        "paintbrush_standard": createImage(resolve(__dirname, "../../../client/src/img/brushes/paintbrush_standard.png")),
        "pencil_rough": createImage(resolve(__dirname, "../../../client/src/img/brushes/pencil_rough.png")),
        "pencil_soft": createImage(resolve(__dirname, "../../../client/src/img/brushes/pencil_soft.png"))
    };

    protected getImage(name: string, color: string): Promise<HTMLImageElement> {
        if (!NodeBrushTool.brushes.hasOwnProperty(name)) {
            return Promise.reject(`Brush "${name}" does not exist.`);
        } else {
            const key = `${name}:${color}`;
            if (this.cache.hasOwnProperty(name)) {
                return Promise.resolve(this.cache[key]);
            } else {
                const canvas = createCanvas(BRUSH_TEXTURE_WIDTH, BRUSH_TEXTURE_HEIGHT);
                const context = canvas.getContext("2d") as CanvasRenderingContext2D;
                context.drawImage(NodeBrushTool.brushes[name], 0, 0);
                context.fillStyle = color;
                context.globalCompositeOperation = "source-in";
                context.fillRect(0, 0, BRUSH_TEXTURE_WIDTH, BRUSH_TEXTURE_HEIGHT);
                const image = createImage(canvas.toDataURL("image/png"));
                this.cache[key] = image;
                return Promise.resolve(image);
            }
        }
    }
}

export async function flatten(canvas: CanvasRecord<Buffer>, history: Array<HistoryRecord>): Promise<Buffer> {
    const destination = createCanvas(canvas.width, canvas.height);
    const destinationCtx = destination.getContext("2d");
    const layers: Layer[] = [];
    for (let i = 0; i < canvas.layers; i++) {
        const draftCanvas = createCanvas(canvas.width, canvas.height);
        const draftCtx = draftCanvas.getContext("2d");
        const finalCanvas = createCanvas(canvas.width, canvas.height);
        const finalCtx = finalCanvas.getContext("2d");
        layers.push({
            id: i,
            draftCanvas: <any>draftCanvas,
            draftCtx,
            finalCanvas: <any>finalCanvas,
            finalCtx
        });
    }
    const tools: { [name: string]: Tool } = {
        brush: new NodeBrushTool(layers),
        circle: new CircleTool(layers),
        colorpicker: new ColorPickerTool(layers, {}, () => {}),
        eraser: new EraserTool(layers, {}, canvas.background_color),
        pencil: new PencilTool(layers),
        rectangle: new RectangleTool(layers)
    };
    await tools.rectangle.draw([
        { x: 0, y: 0},
        { x: canvas.width, y: canvas.height }
    ], {
        strokeStyle: canvas.background_color,
        fillStyle: canvas.background_color,
        layer: 0
    });
    if (canvas.snapshots) {
        canvas.snapshots.forEach((snapshot, i) => {
            const image = createImage(snapshot);
            layers[i].finalCtx.drawImage(image, 0, 0);
        });
    }
    await asyncForEach(history, async (entry, i) => {
        await tools[entry.tool_name].draw(entry.path, entry.settings);
    });
    layers.forEach(layer => {
        let img = new Image();
        img.src = layer.finalCanvas.toDataURL("image/png");
        destinationCtx.drawImage(img, 0, 0, canvas.width, canvas.height);
    });
    return destination.toBuffer();
}