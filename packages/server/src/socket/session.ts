import * as SocketIO from "socket.io";
import * as winston from "winston";
import * as r from "rethinkdb";
import * as redis from "redis";
import { auth } from "../lib/auth";
import { Connection } from "../lib/db";
import { UserRecord, CanvasRecord } from "@drawgaiden/common";

interface SessionArgs {
    sock: SocketIO.Socket;
    db: Connection;
    logger: winston.LoggerInstance;
}

export default function session(args: SessionArgs) {
    const { sock, db, logger } = args;
    let user: UserRecord = null;
    let canvas_id: number = -1;

    const handleErrors = (fn: Function) => {
        return async (...args: any[]) => {
            let cb: Function;
            try {
                await fn.apply(null, args);
            } catch(error) {
                logger.error(error);
                args[args.length - 1]({
                    success: false,
                    errorMessage: error.toString()
                });
            }
        }
    };

    const authCheck = (fn: Function) => {
        return async (...args: any[]) => {
            if (!user) {
                args[args.length - 1]({
                    success: false,
                    errorMessage: "Not authenticated"
                });
            } else {
                await fn.apply(null, args);
            }
        }
    };

    const canvasCheck = (fn: Function) => {
        return async (...args: any[]) => {
            if (canvas_id < 0) {
                args[args.length - 1]({
                    success: false,
                    errorMessage: "Not subscribed to a canvas"
                });
            } else {
                await fn.apply(null, args);
            }
        }
    };

    sock.on("login", handleErrors(async (req: any, cb: Function) => {
        if (user) {
            return cb({
                success: false,
                errorMessage: "Already authenticated"
            });
        }
        user = await auth(req.token, db);
        cb({ success: true, user });
    }));

    sock.on("canvas:create", handleErrors(authCheck(async (req: any, cb: Function) => {
        const canvas = await db.createCanvas({
            ...req.options,
            owner_id: user.id
        });
        cb({
            success: true,
            canvas
        });
    })));

    sock.on("canvas:join", handleErrors(authCheck(async (req: any, cb: Function) => {
        // The typing for r.Cursor can't be coerced to the actual object
        // type you want, so it needs to be converted to any type first.
        let canvas = (await db.getCanvasBySlug(req.slug)) as any as CanvasRecord<Buffer>;
        if (!canvas) {
            cb({
                success: false,
                errorMessage: "Canvas not found"
            });
            return;
        }
        canvas_id = canvas.id;
        sock.join(canvas_id.toString());
        sock.broadcast.to(canvas_id.toString()).emit("canvas:user:join", { user });
        cb({
            success: true,
            canvas,
            history: [],
            users: []
        });
        let offset = 0;
        async function streamHistory() {
            let history = await db.getHistory(canvas.id, offset, 20);
            history.forEach((entry) => {
                sock.emit("canvas:history:new", { entry });
            });
            if (history.length === 20) {
                offset += 20;
                setImmediate(streamHistory);
            }
        }
        streamHistory();
    })));

    sock.on("canvas:leave", handleErrors(authCheck(canvasCheck(async (cb: Function) => {
        canvas_id = -1;
        sock.broadcast.to(canvas_id.toString()).emit("canvas:user:leave", { user });
        sock.leave(canvas_id.toString());
        cb({ success: true });
    }))));

    sock.on("canvas:draw", handleErrors(authCheck(canvasCheck(async (req: any, cb: Function) => {
        const { entry } = req;
        sock.broadcast.to(canvas_id.toString()).emit("canvas:history:new", { entry });
        await db.addHistory(entry);
        cb({ success: true });
    }))));
}