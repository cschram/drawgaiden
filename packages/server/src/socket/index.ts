import * as SocketIO from "socket.io";
import * as redis from "socket.io-redis";
import * as Hapi from "hapi";
import { createLogger } from "../lib/logger";
import { Connection } from "../lib/db";
import session from "./session";
import config from "../lib/config";

const logger = createLogger("socket");

export function attachSocketServer(httpServer: Hapi.Server, db: Connection) {
    const io = SocketIO(httpServer.listener);
    io.adapter(redis({
        host: config.redis.host,
        port: config.redis.port
    }));
    io.on("connection", (sock: SocketIO.Socket) => session({
        sock,
        db,
        logger
    }));
}