import * as Hapi from "hapi";
import * as Boom from "boom";
import * as Joi from "joi";
import { UserRecord } from "@drawgaiden/common";
import { Connection } from "../lib/db";
import { Logger } from "../lib/logger";

export function setup(server: Hapi.Server, db: Connection, logger: Logger) {
    //
    // Users, for the time being, are no longer tied to a canvas, so this route
    // has no purpose.
    //
    // server.route({
    //     method: "GET",
    //     path: "/users",
    //     options: {
    //         validate: {
    //             query: {
    //                 canvasID: Joi.string().optional(),
    //                 skip: Joi.number().optional(),
    //                 limit: Joi.number().optional()
    //             }
    //         }
    //     },
    //     handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
    //         try {
    //             const query = request.query as Hapi.RequestQuery;
    //             const filter: any = {
    //                 skip: query.skip ? parseInt(query.skip as string, 10) : 0,
    //                 limit: query.limit ? parseInt(query.limit as string, 10) : 20
    //             };
    //             if (query.canvasID) {
    //                 filter.canvasID = query.canvasID;
    //             }
    //             const users = await db.getUsers(filter);
    //             return users.toArray();
    //         } catch(error) {
    //             logger.error(error);
    //             return Boom.badImplementation();
    //         }
    //     }
    // });

    server.route({
        method: "GET",
        path: "/user/{id}",
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const user = await db.getUser(parseInt(request.params.id, 10));
                if (user) {
                    return user;
                } else {
                    return Boom.notFound();
                }
            } catch(error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });

    server.route({
        method: "POST",
        path: "/user/{id}",
        options: {
            validate: {
                payload: {
                    username: Joi.string().optional(),
                    avatar: Joi.string().optional()
                }
            }
        },
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const id = parseInt(request.params.id, 10);
                const user = request.auth.credentials as UserRecord;
                if (id !== user.id) {
                    return Boom.forbidden();
                }
                const payload = request.payload as any;
                if (payload.username) {
                    user.username = payload.username;
                }
                if (payload.avatar) {
                    user.avatar = payload.avatar;
                }
                await db.updateUser(user);
                return user;
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });
}