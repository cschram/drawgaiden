import * as Hapi from "hapi";
import * as Boom from "boom";
import * as Joi from "joi";
import { createCanvas, Image } from "canvas";
import * as DrawGaiden from "@drawgaiden/common";
import config from "../lib/config";
import { Connection, CanvasFilter } from "../lib/db";
import { Logger } from "../lib/logger";
import { flatten } from "../lib/canvas";

export function setup(server: Hapi.Server, db: Connection, logger: Logger) {
    /**
     *  Cached Server Methods
     */

    async function generateThumbnail(slug: string, width: number, height: number): Promise<string> {
        const canvas: DrawGaiden.CanvasRecord<Buffer> = await db.getCanvasBySlug(slug);
        if (!canvas) {
            throw Boom.notFound();
        }
        const history: Array<DrawGaiden.HistoryRecord> = await db.getHistory(canvas.id);
        const buffer = await flatten(canvas, history);
        if (width !== canvas.width || height !== canvas.height) {
            const destination = createCanvas(width, height);
            const ctx = destination.getContext("2d");
            const image = new Image();
            let scaleWidth = 0;
            let scaleHeight = 0;
            image.src = buffer;
            if ((width / height) >= 1) {
                scaleHeight = Math.min(canvas.height, height);
                scaleWidth = height * (canvas.width / canvas.height);
            } else {
                scaleWidth = Math.min(canvas.width, width);
                scaleHeight = width * (canvas.height / canvas.width);
            }
            ctx.drawImage(
                image,
                (width / 2) - (scaleWidth / 2),
                (height / 2) - (scaleHeight / 2),
                scaleWidth,
                scaleHeight
            );
            return destination.toBuffer().toString("base64");
        } else {
            return buffer.toString("base64");
        }
    }

    server.method("generateThumbnail", generateThumbnail, {
        cache: {
            expiresIn: config.cache.thumbnailTTL,
            generateTimeout: 10000
        }
    });

    /**
     * Server Routes
     */

    server.route({
        method: "GET",
        path: "/canvas",
        options: {
            validate: {
                query: {
                    owner_id: Joi.number().optional(),
                    title: Joi.string().optional(),
                    skip: Joi.number().optional(),
                    limit: Joi.number().optional()
                }
            }
        },
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const user = request.auth.credentials as DrawGaiden.UserRecord;
                const query = request.query as Hapi.RequestQuery;
                const skip = query.skip ? parseInt(query.skip as string, 10) : 0;
                const limit = query.limit ? parseInt(query.limit as string, 10) : 20;
                let filter: CanvasFilter = {};
                if (query.owner_id) {
                    const owner_id = parseInt(<string>query.owner_id, 10);
                    filter.owner_id = owner_id;
                    if (owner_id !== user.id) {
                        filter.is_public = true;
                    }
                } else {
                    filter.is_public = true;
                }
                if (query.title) {
                    filter.title = decodeURIComponent(query.title as string);
                }
                const canvases = await db.getCanvases(filter, skip, limit);
                return canvases;
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });

    server.route({
        method: "POST",
        path: "/canvas",
        options: {
            validate: {
                payload: {
                    title: Joi.string().required(),
                    is_public: Joi.boolean().required(),
                    width: Joi.number().required(),
                    height: Joi.number().required(),
                    layers: Joi.number().required(),
                    background_color: Joi.string().required()
                }
            }
        },
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const user = request.auth.credentials as DrawGaiden.UserRecord;
                const options = request.payload as DrawGaiden.CanvasData;
                const canvasCount = await db.getOwnerCanvasCount(user.id);
                const canvas = await db.createCanvas({
                    ...options,
                    owner_id: user.id
                });
                return canvas;
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });

    server.route({
        method: "GET",
        path: "/canvas/{slug}",
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const canvas: DrawGaiden.CanvasRecord<Buffer> = await db.getCanvasBySlug(request.params.slug);
                if (!canvas) {
                    return Boom.notFound();
                }
                return canvas;
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });

    server.route({
        method: "GET",
        path: "/canvas/{slug}/history",
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const canvas: DrawGaiden.CanvasRecord<Buffer> = await db.getCanvasBySlug(request.params.slug);
                if (!canvas) {
                    return Boom.notFound();
                }
                const history: Array<DrawGaiden.HistoryRecord> = await db.getHistory(canvas.id);
                return history;
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });

    server.route({
        method: "GET",
        path: "/canvas/{slug}/screenshot.png",
        options: {
            auth: false
        },
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const canvas: DrawGaiden.CanvasRecord<Buffer> = await db.getCanvasBySlug(request.params.slug);
                if (!canvas) {
                    return Boom.notFound();
                }
                const history: Array<DrawGaiden.HistoryRecord> = await db.getHistory(canvas.id);
                const image = await flatten(canvas, history);
                const response = h.response(image);
                response.type("image/png");
                return response;
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });

    server.route({
        method: "GET",
        path: "/canvas/{slug}/thumbnail.png",
        options: {
            auth: false,
            cache: {
                expiresIn: config.cache.thumbnailTTL
            },
            validate: {
                query: {
                    width: Joi.number().required(),
                    height: Joi.number().required()
                }
            }
        },
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const query = request.query as Hapi.RequestQuery;
                const image = await server.methods.generateThumbnail(
                    request.params.slug,
                    parseInt(query["width"] as string, 10),
                    parseInt(query["height"] as string, 10)
                );
                const buffer = Buffer.from(image, "base64");
                const response = h.response(buffer);
                response.type("image/png");
                return response;
            } catch (error) {
                if (Boom.isBoom(error)) {
                    return error;
                } else {
                    logger.error(error);
                    return Boom.badImplementation();
                }
            }
        }
    });

    server.route({
        method: "DELETE",
        path: "/canvas/{slug}",
        handler: async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            try {
                const canvas = await db.getCanvasBySlug(request.params.slug);
                await db.deleteCanvas(canvas.id);
                return {};
            } catch (error) {
                logger.error(error);
                return Boom.badImplementation();
            }
        }
    });
}