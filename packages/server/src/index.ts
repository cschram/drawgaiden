import * as Hapi from "hapi";
import { createLogger } from "./lib/logger";
import { auth } from "./lib/auth";
import { Connection } from "./lib/db";
import config from "./lib/config";
import { HealthMonitor } from "./lib/healthmonit";
import { attachSocketServer } from "./socket";

let port = config.server.port;
if (process.env.NODE_APP_INSTANCE) {
    port += parseInt(process.env.NODE_APP_INSTANCE, 10);
}

const logger = createLogger("http");
const server = new Hapi.Server({
    host: config.server.host,
    port,
    cache: [
        {
            name: "redis_cache",
            engine: require("catbox-redis"),
            host: config.redis.host,
            port: config.redis.port,
            partition: "cache"
        }
    ]
});
const monitor = new HealthMonitor(server);

async function start() {
    try {
        const db = new Connection();

        await server.register(require("hapi-auth-bearer-token"));
        server.auth.strategy("simple", "bearer-access-token", {
            validate: async (request: Hapi.Request, token: string, h: Hapi.ResponseToolkit) => {
                try {
                    const credentials = await auth(token, db);
                    return { isValid: true, credentials };
                } catch (error) {
                    return { isValid: false };
                }
            }
        });
        server.auth.default("simple");

        // Import and run REST API modules
        const modules = require("require-dir")("./rest");
        Object.keys(modules).forEach(moduleName => {
            logger.info(`Loading REST module ${moduleName}`);
            const module = modules[moduleName];
            module.setup(server, db, logger);
        });

        // Attach socket server
        attachSocketServer(server, db);

        // Start the server
        await server.start();
        logger.info(`Server listening on ${config.server.host}:${port}`);
    } catch (error) {
        if (error.stack) {
            logger.error(error.stack);
        } else {
            logger.error(error);
        }
        monitor.error(error.toString());
    }
}

start();