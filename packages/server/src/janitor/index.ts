// import * as Canvas from "canvas";
import { Canvas, Image, createCanvas } from "canvas";
import * as Hapi from "hapi";
import pMap from "p-map";
import pEachSeries from "p-each-series";
import { CanvasRecord } from "@drawgaiden/common";
import { Tool } from "@drawgaiden/easel/lib/tools/tool";
import CircleTool from "@drawgaiden/easel/lib/tools/circle";
import ColorPickerTool from "@drawgaiden/easel/lib/tools/colorpicker";
import EraserTool from "@drawgaiden/easel/lib/tools/eraser";
import PencilTool from "@drawgaiden/easel/lib/tools/pencil";
import RectangleTool from "@drawgaiden/easel/lib/tools/rectangle";
import { Layer } from "@drawgaiden/easel/lib/util";
import { Connection } from "../lib/db";
import { NodeBrushTool } from "../lib/canvas";
import config from "../lib/config";
import { HealthMonitor } from "../lib/healthmonit";
import { createLogger } from "../lib/logger";

const logger = createLogger("janitor");
const server = new Hapi.Server({
    port: config.janitor.monitorPort
});
const monitor = new HealthMonitor(server);

function reportError(error: any) {
    if (error.stack) {
        logger.error(error.stack);
    } else {
        logger.error(error);
    }
}

// Squash a canvases history into a snapshot.
async function squash(db: Connection, canvas_id: number) {
    const canvas_record: CanvasRecord<Buffer> = await db.getCanvas(canvas_id);
    const layers: Layer[] = [];
    for (let i = 0; i < canvas_record.layers; i++) {
        const draftCanvas = createCanvas(canvas_record.width, canvas_record.height);
        const draftCtx = draftCanvas.getContext("2d");
        const finalCanvas = createCanvas(canvas_record.width, canvas_record.height);
        const finalCtx = finalCanvas.getContext("2d");
        layers.push({
            id: i,
            draftCanvas: <any>draftCanvas,
            draftCtx,
            finalCanvas: <any>finalCanvas,
            finalCtx
        });
    }
    const tools: { [name: string]: Tool } = {
        brush: new NodeBrushTool(layers),
        circle: new CircleTool(layers),
        colorpicker: new ColorPickerTool(layers, {}, () => {}),
        eraser: new EraserTool(layers, {}, canvas_record.background_color),
        pencil: new PencilTool(layers),
        rectangle: new RectangleTool(layers)
    };
    await tools.rectangle.draw([
        { x: 0, y: 0},
        { x: canvas_record.width, y: canvas_record.height }
    ], {
        strokeStyle: canvas_record.background_color,
        fillStyle: canvas_record.background_color,
    });
    if (canvas_record.snapshots) {
        canvas_record.snapshots.forEach((snapshot, i) => {
            let img = new Image();
            img.src = snapshot;
            layers[i].finalCtx.drawImage(<any>img, 0, 0);
        });
    }
    // const historyCursor = await db.getHistory(canvas_id);
    // const history: DrawGaiden.HistoryEntry[] = await historyCursor.toArray();
    // await asyncForEach(history, async (entry, i) => {
    //     await tools[entry.toolName].draw(entry.path, entry.settings);
    // });
    await new Promise((resolve, reject) => {
        let offset = 0;
        async function streamHistory() {
            const history = await db.getHistory(canvas_id, offset, 20);
            await pEachSeries(history, async (entry, i) => {
                await tools[entry.tool_name].draw(entry.path, entry.settings);
            });
            if (history.length === 20) {
                offset += 20;
                setImmediate(streamHistory);
            } else {
                resolve();
            }
        }
    });
    // It would be nice if this set of operations could be made atomic, but worst case scenario
    // the user gets the snapshot *and* the squashed entries, causing the app to needlessly
    // redraw entries in the snapshot.
    canvas_record.snapshots = layers.map(layer => {
        const canvas: Canvas = layer.finalCanvas as unknown as Canvas;
        return canvas.toBuffer();
    });
    await db.updateCanvas(canvas_record);
    // We want to limit the number of entries we're clearing because some may have been created while
    // the operation was running.
    await db.clearHistory(canvas_id, history.length);
    logger.info(`Squashed canvas "${canvas_id}"`);
}

async function cleanUpHistory(db: Connection, id: number) {
    let historyCount = await db.getHistoryCount(id);
    if (historyCount > config.janitor.historyThreshold) {
        await squash(db, id);
    }
}

async function start() {
    try {
        const db = new Connection();

        async function cleanUp() {
            logger.info("Performing clean up");
            const canvas_ids = await db.getCanvasIds();
            pMap(canvas_ids, async (id) => {
                try {
                    await cleanUpHistory(db, id);
                } catch(error) {
                    logger.error(error.stack);
                }
            }, { concurrency: 4 });
            setTimeout(cleanUp, config.janitor.jobInterval);
        }

        await server.start();
        cleanUp();
        logger.info("Janitor running");
    } catch (error) {
        reportError(error);
        monitor.error(error.toString());
    }
}

start();