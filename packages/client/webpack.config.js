const webpack = require("webpack");
const path = require("path");
const DotenvPlugin = require("dotenv-webpack");

module.exports = {
    devServer: {
        historyApiFallback: true,
        hot: true,
        inline: true,
        contentBase: "./src",
        port: 8000,
        proxy: {
            "/api": {
                target: "http://localhost:9000",
                pathRewrite: {
                    "^/api": ""
                }
            },
            "/socket.io": {
                target: "ws://localhost:9000",
                ws: true
            }
        }
    },
    entry: path.resolve(__dirname, "src/main.tsx"),
    output: {
        path: path.resolve(__dirname, "dist"),
        publicPath: "/",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.ts[x]?$/,
                loader: ["babel-loader", "ts-loader"]
            },
            {
                test: /\.[s]?css$/,
                loader: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "sass-loader"
                    }
                ]
            },
            {
                test: /\.(png)$/,
                loader: "file-loader"
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new DotenvPlugin()
    ]
};