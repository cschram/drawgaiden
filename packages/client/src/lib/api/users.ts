import axios from "axios";
import { UserRecord } from "@drawgaiden/common";

export async function getUser(id: number, token: string): Promise<UserRecord> {
    const response = await axios.get(`/api/user/${id}`, {
        headers: {
            "Authorization": `Bearer ${token}`
        }
    });
    return response.data;
}

export async function updateUser(user: UserRecord, token: string): Promise<UserRecord> {
    const response = await axios.post(`/api/user/${user.id}`, {
        username: user.username,
        avatar: user.avatar
    }, {
        headers: {
            "Authorization": `Bearer ${token}`
        }
    });
    return response.data;
}