import axios from "axios";
import { CanvasRecord, CanvasOptions } from "@drawgaiden/common";

interface GetCanvasParams {
    owner_id?: number;
    skip?: number;
    limit?: number;
}

export async function getCanvases(params: GetCanvasParams, token: string): Promise<Array<CanvasRecord<string>>> {
    params.skip = params.skip || 0;
    params.limit = params.limit || 20;
    const response = await axios.get("/api/canvas", {
        params,
        headers: {
            "Authorization": `Bearer ${token}`
        }
    });
    return response.data;
}

export async function createCanvas(options: CanvasOptions, token: string): Promise<CanvasRecord<string>> {
    const response = await axios.post("/api/canvas", options, {
        headers: {
            "Authorization": `Bearer ${token}`
        }
    });
    return response.data;
}

export async function deleteCanvas(slug: string, token: string): Promise<void> {
    await axios.delete(`/api/canvas/${slug}`, {
        headers: {
            "Authorization": `Bearer ${token}`
        }
    });
}