export function getStorageItem(name: string) {
    if (window.localStorage) {
        return localStorage.getItem(name);
    }
    return null;
}

export function setStorageItem(name: string, value: string) {
    if (window.localStorage) {
        localStorage.setItem(name, value);
    }
}

export function removeStorageItem(name: string) {
    if (window.localStorage) {
        localStorage.removeItem(name);
    }
}