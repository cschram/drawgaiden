import { push } from "react-router-redux";
import { getStorageItem, setStorageItem, removeStorageItem } from "../lib/storage";
import { socketLogin } from "./connection";

export function login(redirect?: string) {
    return (dispatch, getState) => {
        const client = getState().auth.client;
        removeStorageItem("authRedirect");
        if (redirect) {
            setStorageItem("authRedirect", redirect);
        }
        client.authorize();
    }
}

export function logout() {
    return { type: "LOGOUT" };
}

export function handleCallback() {
    return (dispatch, getState) => {
        const state = getState();
        const client = state.auth.client;
        const socket = state.connection.socket;
        client.parseHash(async (error, payload) => {
            if (error) {
                console.error(error);
            } else {
                await dispatch(socketLogin(socket, payload.accessToken, payload.idToken, (payload.expiresIn * 1000) + Date.now()));
                const redirect = getStorageItem("authRedirect");
                if (redirect) {
                    removeStorageItem("authRedirect");
                    dispatch(push(redirect));
                } else {
                    dispatch(push("/"));
                }
            }
        });
    };
}