import { push } from "react-router-redux";
import {
    CanvasRecord,
    CanvasOptions,
    HistoryRecord,
    Coord
} from "@drawgaiden/common";
import * as API from "../lib/api";

export function loadCanvases(ownerID: string, search: string = "") {
    return async (dispatch, getState) => {
        const token = getState().auth.accessToken;
        const canvasList = getState().canvasList;
        let skip = 0;
        if (canvasList.ownerID !== ownerID || canvasList.search !== search) {
            dispatch({
                type: "LOADING_CANVAS_LIST",
                payload: {
                    ownerID,
                    search
                }
            });
        } else {
            skip = canvasList.canvases.length;
            dispatch({ type: "LOADING_MORE_CANVAS_LIST" });
        }
        const params: any = {
            skip,
            limit: 20
        };
        if (ownerID.length > 0) {
            params.ownerID = ownerID;
        }
        if (search.length > 0) {
            params.title = encodeURIComponent(search);
        }
        const canvases = await API.getCanvases(params, token);
        dispatch({
            type: "LOADED_CANVAS_LIST",
            payload: canvases
        });
    };
}

export function createCanvas(options: CanvasOptions) {
    return async (dispatch, getState) => {
        const token = getState().auth.accessToken;
        dispatch({ type: "CREATING_CANVAS" });
        try {
            const canvas = await API.createCanvas(options, token);
            dispatch({ type: "CREATED_CANVAS" });
            dispatch(push(`/canvas/${canvas.slug}`));
        } catch (error) {
            dispatch({
                type: "CREATING_CANVAS_FAILED",
                payload: error.response ? error.response.data.message : error.message
            });
        }
    }
}

export function deleteCanvas(canvas: CanvasRecord<string>) {
    return async (dispatch, getState) => {
        const token = getState().auth.accessToken;
        dispatch({
            type: "DELETING_CANVAS",
            payload: canvas.id
        });
        await API.deleteCanvas(canvas.slug, token);
        dispatch({
            type: "DELETED_CANVAS",
            payload: canvas.id
        });
    }
}

export function joinCanvas(slug: string) {
    return (dispatch, getState) => {
        dispatch({ type: "JOIN_CANVAS_STARTED" });
        let socket = getState().connection.socket;
        socket.emit("canvas:join", { slug }, (resp: any) => {
            if (resp.success) {
                dispatch({
                    type: "JOIN_CANVAS_SUCCESS",
                    payload: {
                        canvas: resp.canvas,
                        history: resp.history,
                        users: resp.users
                    }
                });
            } else {
                dispatch(push("/404"));
            }
        });
    };
}

export function leaveCanvas() {
    return (dispatch, getState) => {
        dispatch({ type: "LEAVE_CANVAS" });
    };
}

export function draw(entry: HistoryRecord) {
    return (dispatch, getState) => {
        let socket = getState().connection.socket;
        socket.emit("canvas:draw", { entry }, (resp: any) => {
            if (!resp.success) {
                console.error(resp.errorMessage);
            }
        });
    }
}

export function setMousePosition(coord: Coord) {
    return (dispatch, getState) => {
        let socket = getState().connection.socket;
        socket.emit("user:position:set", { coord }, (resp: any) => {
            if (!resp.success) {
                console.error(resp.errorMessage);
            }
        });
    }
}

export function changeTool(tool: string) {
    return {
        type: "CANVAS_CHANGE_TOOL",
        payload: tool
    };
}

export function changeLayer(layer: number) {
    return {
        type: "CANVAS_CHANGE_LAYER",
        payload: layer
    };
}

export function changeStrokeColor(color: string) {
    return {
        type: "CANVAS_CHANGE_STROKE_COLOR",
        payload: color
    };
}

export function changeFillColor(color: string) {
    return {
        type: "CANVAS_CHANGE_FILL_COLOR",
        payload: color
    };
}

export function changeToolSize(size: number) {
    return {
        type: "CANVAS_CHANGE_TOOL_SIZE",
        payload: size
    };
}

export function changeToolOpacity(opacity: number) {
    return {
        type: "CANVAS_CHANGE_TOOL_OPACITY",
        payload: opacity
    };
}

export function changeToolSetting<T>(setting: string, value: T) {
    return {
        type: "CANVAS_CHANGE_TOOL_SETTING",
        payload: {
            setting,
            value
        }
    };
}