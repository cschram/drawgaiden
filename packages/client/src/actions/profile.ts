import { push } from "react-router-redux";
import { UserRecord } from "@drawgaiden/common";
import * as API from "../lib/api";

export function loadProfile(id: number) {
    return async (dispatch, getState) => {
        const state = getState();
        const token = state.auth.accessToken;
        dispatch({ type: "LOADING_PROFILE" });
        try {
            const user = await API.getUser(id, token);
            const canvases = await API.getCanvases({
                owner_id: id,
                skip: 0,
                limit: parseInt(process.env.CANVAS_LIMIT, 10)
            }, token);
            dispatch({
                type: "LOADED_PROFILE",
                payload: {
                    user,
                    canvases
                }
            });
        } catch (error) {
            if (error.response && error.response.status === 404) {
                dispatch(push("/404"));
            } else {
                console.error(error);
            }
        }
    };
}

export function updateProfile(user: UserRecord) {
    return async (dispatch, getState) => {
        const token = getState().auth.accessToken;
        try {
            await API.updateUser(user, token);
            dispatch({
                type: "UPDATED_PROFILE",
                payload: user
            });
        } catch (error) {
            console.error(error);
        }
    }
}