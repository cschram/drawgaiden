import io from "socket.io-client";

export function socketLogin(socket: SocketIOClient.Socket, accessToken: string, idToken: string, expiresAt: number) {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            socket.emit("login", { token: accessToken }, (resp: any) => {
                if (resp.success) {
                    dispatch({
                        type: "LOGIN_SUCCESS",
                        payload: {
                            accessToken,
                            idToken,
                            expiresAt,
                            user: resp.user
                        }
                    });
                } else {
                    console.error(resp.errorMessage);
                    dispatch({ type: "LOGOUT" });
                }
                resolve();
            });
        });
    };
}

export function connect() {
    return (dispatch, getState) => {
        dispatch({ type: "CONNECTING" });
        const socket = io();

        socket.on("connect", async () => {
            let { accessToken, idToken, expiresAt } = getState().auth;
            if (accessToken && Date.now() < expiresAt) {
                await dispatch(socketLogin(socket, accessToken, idToken, expiresAt));
            }
            dispatch({
                type: "CONNECTED",
                payload: socket
            });
        });

        socket.on("disconnect", () => {
            dispatch({ type: "DISCONNECTED" });
        });

        socket.on("canvas:history:new", (event: any) => {
            dispatch({
                type: "CANVAS_HISTORY_NEW",
                payload: event.entry
            });
        });
    }
}