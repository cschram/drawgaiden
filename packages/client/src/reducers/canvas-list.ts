import { CanvasRecord } from "@drawgaiden/common";

interface CanvasListState {
    canvases: Array<CanvasRecord<string>>;
    owner_id: string;
    search: string;
    loading: boolean;
    loaded: boolean;
}

const initialState: CanvasListState = {
    canvases: [],
    owner_id: "",
    search: "",
    loading: false,
    loaded: false
};

export default function(state = initialState, { type, payload }): CanvasListState {
    switch (type) {
        case "LOADING_CANVAS_LIST":
            return {
                ...state,
                canvases: [],
                owner_id: payload.owner_id,
                search: payload.search,
                loading: true,
            };

        case "LOADING_MORE_CANVAS_LIST":
            return {
                ...state,
                loading: true
            };

        case "LOADED_CANVAS_LIST":
            return {
                ...state,
                canvases: state.canvases.concat(payload),
                loading: false,
                loaded: true
            };

        case "DELETED_CANVAS":
            return {
                ...state,
                canvases: state.canvases.filter(canvas => canvas.id !== payload)
            };

        default:
            return state;
    }
}