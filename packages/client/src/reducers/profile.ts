import { UserRecord, CanvasRecord } from "@drawgaiden/common";

interface ProfileState {
    loading: boolean;
    user: UserRecord;
    canvases: Array<CanvasRecord<string>>;
}

const initialState: ProfileState = {
    loading: false,
    user: null,
    canvases: []
};

export default function (state = initialState, { type, payload }): ProfileState {
    switch (type) {
        case "LOADING_PROFILE":
            return {
                ...initialState,
                loading: true
            };

        case "LOADED_PROFILE":
            return {
                loading: false,
                user: payload.user,
                canvases: payload.canvases
            };

        case "UPDATED_PROFILE":
            return {
                loading: false,
                user: payload,
                canvases: state.canvases.map(canvas => ({
                    ...canvas,
                    owner: payload
                }))
            };

        case "DELETED_CANVAS":
            return {
                ...state,
                canvases: state.canvases.filter(canvas => canvas.id !== payload)
            };

        default:
            return state;
    }
}