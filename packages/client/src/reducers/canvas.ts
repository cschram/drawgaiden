import {
    CanvasRecord,
    HistoryRecord,
    UserRecord
} from "@drawgaiden/common";
import { getStorageItem, setStorageItem } from "../lib/storage";
const airBrushSrc = require("../img/brushes/airbrush.png");

interface CanvasToolSettings {
    size: number;
    opacity: number;
    smoothLines: boolean;
    brush: string;
}

interface CanvasState {
    creatingCanvas: boolean;
    creatingCanvasError: string;
    lastCanvasSlug: string;
    canvas: CanvasRecord<string>;
    latestEntry: HistoryRecord;
    loading: boolean;
    currentTool: string;
    currentLayer: number;
    strokeColor: string;
    fillColor: string;
    toolSettings: {
        [toolName: string]: CanvasToolSettings;
    };
}

const initialState: CanvasState = {
    creatingCanvas: false,
    creatingCanvasError: "",
    lastCanvasSlug: getStorageItem("lastCanvasSlug") || "",
    canvas: null,
    latestEntry: null,
    loading: false,
    currentTool: "pencil",
    currentLayer: 0,
    strokeColor: "#000000",
    fillColor: "#ffffff",
    toolSettings: {
        brush: {
            size: 20,
            opacity: 80,
            smoothLines: true,
            brush: "airbrush"
        },
        circle: {
            size: 1,
            opacity: 100,
            smoothLines: true,
            brush: "airbrush"
        },
        colorpicker: {
            size: 1,
            opacity: 100,
            smoothLines: true,
            brush: "airbrush"
        },
        eraser: {
            size: 10,
            opacity: 100,
            smoothLines: true,
            brush: "airbrush"
        },
        pencil: {
            size: 1,
            opacity: 100,
            smoothLines: true,
            brush: "airbrush"
        },
        rectangle: {
            size: 1,
            opacity: 100,
            smoothLines: true,
            brush: "airbrush"
        }
    }
};

export default function(state = initialState, { type, payload }): CanvasState {
    switch (type) {
        case "CREATING_CANVAS":
            return {
                ...state,
                creatingCanvas: true,
                creatingCanvasError: ""
            };

        case "CREATED_CANVAS":
            return {
                ...state,
                creatingCanvas: false
            };

        case "CREATING_CANVAS_FAILED":
            return {
                ...state,
                creatingCanvas: false,
                creatingCanvasError: payload
            };

        case "JOIN_CANVAS_STARTED":
            return Object.assign({}, initialState, {
                loading: true
            });

        case "JOIN_CANVAS_SUCCESS":
            setStorageItem("lastCanvasSlug", payload.canvas.slug);
            return Object.assign({}, initialState, {
                lastCanvasSlug: payload.canvas.slug,
                canvas: payload.canvas,
                users: payload.users,
                loading: false
            });

        case "LEAVE_CANVAS":
            return Object.assign({}, state, {
                canvas: null,
                latestEntry: null,
                users: []
            });

        case "CANVAS_HISTORY_NEW":
            return Object.assign({}, state, {
                latestEntry: payload
            });
            
        case "CANVAS_CHANGE_TOOL":
            return Object.assign({}, state, {
                currentTool: payload
            });

        case "CANVAS_CHANGE_LAYER":
            return Object.assign({}, state, {
                currentLayer: payload
            });

        case "CANVAS_CHANGE_STROKE_COLOR":
            return Object.assign({}, state, {
                strokeColor: payload
            });

        case "CANVAS_CHANGE_FILL_COLOR":
            return Object.assign({}, state, {
                fillColor: payload
            });

        case "CANVAS_CHANGE_TOOL_SETTING":
            return {
                ...state,
                toolSettings: {
                    ...state.toolSettings,
                    [state.currentTool]: {
                        ...state.toolSettings[state.currentTool],
                        [payload.setting]: payload.value
                    }
                }
            };

        default:
            return state;
    }
}