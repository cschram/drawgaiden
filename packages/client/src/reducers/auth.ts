import auth0 from "auth0-js";
import { UserRecord } from "@drawgaiden/common";
import { getStorageItem, setStorageItem, removeStorageItem } from "../lib/storage";

interface AuthState {
    client: auth0.WebAuth;
    accessToken: string;
    idToken: string;
    expiresAt: number;
    user: UserRecord;
}

const initialState: AuthState = {
    client: new auth0.WebAuth({
        domain: "drawgaiden.auth0.com",
        clientID: process.env.NODE_ENV === "production" ?
            "3q9A379hJv67jGe8QJYU8AGgzjW6ilGa" :
            "t4mRUX9QgDdmwd6OexXpzu48db7edgwc",
        redirectUri: `${window.location.protocol}//${window.location.host}/callback`,
        audience: `https://drawgaiden.auth0.com/userinfo`,
        responseType: "token id_token",
        scope: "openid profile"
    }),
    accessToken: getStorageItem("accessToken") || "",
    idToken: getStorageItem("idToken") || "",
    expiresAt: parseInt(getStorageItem("expiresAt") || "0", 10),
    user: null
};

export default function(state = initialState, action: { type, payload? }): AuthState {
    const { type, payload } = action;
    switch (type) {
        case "LOGIN_SUCCESS":
            setStorageItem("accessToken", payload.accessToken);
            setStorageItem("idToken", payload.idToken);
            setStorageItem("expiresAt", payload.expiresAt);
            return {
                ...state,
                accessToken: payload.accessToken,
                idToken: payload.idToken,
                expiresAt: payload.expiresAt,
                user: payload.user
            };

        case "UPDATED_PROFILE":
            return {
                ...state,
                user: payload
            };

        case "LOGOUT":
            removeStorageItem("accessToken");
            removeStorageItem("idToken");
            removeStorageItem("expiresAt");
            return {
                ...state,
                accessToken: "",
                idToken: "",
                expiresAt: 0,
                user: null
            };

        default:
            return state;
    }
}