import * as React from "react";
import { UserRecord } from "@drawgaiden/common";
import "./style.scss";

interface AvatarProps {
    user: UserRecord;
    height?: string;
    width?: string;
}

export default function Avatar({ user, height = "40px", width = "40px" }: AvatarProps) {
    return (
        <div className="avatar" style={{ height, width }}>
            <img
                src={user.avatar}
                title={user.username}
            />
        </div>
    );
}