import * as React from "react";
import "./style.scss";

interface CheckboxProps {
    name: string;
    checked: boolean;
    onChange(checked: boolean): void;
    disabled?: boolean;
    className?: string;
    innerRef?: (el: HTMLInputElement) => void;
}

export default class Checkbox extends React.Component<CheckboxProps> {
    onChange = (e) => {
        this.props.onChange(e.target.checked);
    };

    render() {
        const {
            name,
            checked,
            disabled = false,
            className = ""
        } = this.props;
        return (
            <div className={`checkbox ${className}`}>
                <input
                    type="checkbox"
                    id={name}
                    name={name}
                    checked={checked}
                    onChange={this.onChange}
                    disabled={disabled}
                    ref={this.props.innerRef}
                />
                <label htmlFor={name}></label>
                <div className="checkbox__label">{this.props.children}</div>
            </div>
        );
    }
}