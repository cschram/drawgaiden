import React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import { debounce } from "lodash";
import { UserRecord, CanvasRecord } from "@drawgaiden/common";
import { loadCanvases, deleteCanvas } from "../../actions/canvas";
import Button from "../button";
import CanvasList from "../canvas-list"
import Header from "../header";
import Input from "../input";
import Loading from "../loading";;
import Modal from "../modal";
import { Page, PageContent } from "../page";
import "./style.scss";

const SCROLL_LOAD_OFFSET = 50;

interface IndexPageProps {
    user: UserRecord;
    canvases: Array<CanvasRecord<string>>;
    search: string;
    loading: boolean;
    loadCanvases(owner_id: number, search: string): void;
    joinCanvas(slug: string): void;
    deleteCanvas(canvas: CanvasRecord<string>): void;
}

interface IndexPageState {
    owner_id: number;
    canvasToDelete: CanvasRecord<string>;
}

export class IndexPage extends React.Component<IndexPageProps, IndexPageState> {
    constructor(props) {
        super(props);
        this.state = {
            owner_id: -1,
            canvasToDelete: null
        };
    }

    private content: HTMLElement;

    componentDidMount() {
        this.loadCanvases();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.owner_id !== this.state.owner_id) {
            this.loadCanvases();
        }
    }

    componentWillUnmount() {
        if (this.content) {
            this.content.removeEventListener("scroll", this.onScroll);
        }
    }

    loadCanvases = () => {
        this.props.loadCanvases(this.state.owner_id, this.props.search);
    };

    updateSearch = debounce((search: string) => {
        this.props.loadCanvases(this.state.owner_id, search);
    }, 250);

    selectPublicTab = () => {
        this.setState({ owner_id: -1 });
    };

    selectOwnedTab = () => {
        this.setState({ owner_id: this.props.user.id });
    };

    onContentRef = (el: HTMLElement) => {
        if (el) {
            this.content = el;
            this.content.addEventListener("scroll", this.onScroll);
        } else {
            if (this.content) {
                this.content.removeEventListener("scroll", this.onScroll);
            }
            this.content = null;
        }
    };

    onScroll = debounce(() => {
        if (!this.props.loading) {
            const diff = this.content.scrollHeight - (this.content.scrollTop + this.content.clientHeight);
            if (diff <= SCROLL_LOAD_OFFSET) {
                this.loadCanvases();
            }
        }
    }, 250);

    deleteCanvas = (canvas: CanvasRecord<string>) => {
        this.setState({ canvasToDelete: canvas });
    };

    cancelDeleteCanvas = () => {
        this.setState({ canvasToDelete: null });
    };

    confirmDeleteCanvas = () => {
        this.props.deleteCanvas(this.state.canvasToDelete);
        this.setState({ canvasToDelete: null });
    };

    render() {
        const { loading, user, canvases, search } = this.props;
        const { owner_id } = this.state;
        return (
            <Page>
                <Header>
                    <Input
                        className="index-page__search"
                        name="search"
                        value={search}
                        placeholder={"Search..."}
                        onChange={this.updateSearch}
                    />
                </Header>
                <PageContent>
                    <div className="index-page">
                        <ul className="index-page__tabs">
                            <li
                                className={owner_id < 0 ? "active" : ""}
                                onClick={this.selectPublicTab}
                            >
                                Public Canvases
                            </li>
                            <li
                                className={owner_id < 0 ? "" : "active"}
                                onClick={this.selectOwnedTab}
                            >
                                My Canvases
                            </li>
                        </ul>
                        <div className="index-page__content" ref={this.onContentRef}>
                            {loading && canvases.length === 0 ? (
                                <Loading />
                            ) : (
                                <CanvasList
                                    user={user}
                                    canvases={this.props.canvases}
                                    loadMore={this.loadCanvases}
                                    joinCanvas={this.props.joinCanvas}
                                    deleteCanvas={this.deleteCanvas}
                                />
                            )}
                        </div>
                        <Modal
                            title={this.state.canvasToDelete ? this.state.canvasToDelete.title : ""}
                            open={!!this.state.canvasToDelete}
                            onClose={this.cancelDeleteCanvas}
                            height={175}
                        >
                            <p>Are you sure you want to delete this canvas?</p>
                            <Button warning={true} onClick={this.confirmDeleteCanvas}>Confirm</Button>
                            <Button onClick={this.cancelDeleteCanvas}>Cancel</Button>
                        </Modal>
                    </div>
                </PageContent>
            </Page>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    canvases: state.canvasList.canvases,
    search: state.canvasList.search,
    loading: state.canvasList.loaded ? state.canvasList.loading : true
});

const mapDispatchToProps = (dispatch) => ({
    loadCanvases(owner_id: string, search: string) {
        dispatch(loadCanvases(owner_id, search));
    },
    joinCanvas(slug: string) {
        dispatch(push(`/canvas/${slug}`));
    },
    deleteCanvas(canvas: CanvasRecord<string>) {
        dispatch(deleteCanvas(canvas));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);