import * as React from "react";
import "./style.scss";

export function Page({ children }) {
    return <div className="page">{children}</div>;
}

export function PageContent({ children }) {
    return (
        <div className="page__content">
            <div className="page__content-inner">{children}</div>
        </div>
    );
}