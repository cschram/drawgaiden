import React from "react";

function Icon({ name, className = "" }) {
    return <i className={`fa fa-${name} ${className}`} aria-hidden="true"></i>;
}

export default Icon;