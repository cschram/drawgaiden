import * as React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import { distanceInWords } from "date-fns";
import { UserRecord, CanvasRecord } from "@drawgaiden/common";
import { deleteCanvas } from "../../actions/canvas";
import { loadProfile, updateProfile } from "../../actions/profile";
import Avatar from "../avatar";
import Button from "../button";
import CanvasList from "../canvas-list";
import Header from "../header";
import Icon from "../icon";
import Input from "../input";
import Loading from "../loading";
import Modal from "../modal";
import { Page, PageContent } from "../page";
import "./style.scss";

interface ProfilePageProps {
    self: UserRecord;
    loading: boolean;
    user: UserRecord;
    canvases: Array<CanvasRecord<string>>;
    loadProfile(): void;
    updateProfile(user: UserRecord): void;
    joinCanvas(slug: string): void;
    deleteCanvas(canvas: CanvasRecord<string>): void;
}

interface ProfilePageState {
    editingUsername: boolean;
    username: string;
    editingAvatar: boolean;
    avatar: string;
    canvasToDelete: CanvasRecord<string>;
}

class ProfilePage extends React.Component<ProfilePageProps, ProfilePageState> {
    constructor(props) {
        super(props);
        this.state = {
            editingUsername: false,
            username: "",
            editingAvatar: false,
            avatar: "",
            canvasToDelete: null
        };
    }

    componentDidMount() {
        if (!this.props.user && !this.props.loading) {
            this.props.loadProfile();
        }
    }

    componentDidUpdate() {
        if (!this.props.user && !this.props.loading) {
            this.props.loadProfile();
        }
    }

    editUsername = (e) => {
        e.preventDefault();
        this.setState({
            editingUsername: true,
            username: this.props.user.username,
            editingAvatar: false,
            avatar: "",
            canvasToDelete: null
        });
    };

    changeUsername = (username: string) => {
        this.setState({ username });
    };

    submitUsername = () => {
        const user = {
            ...this.props.user,
            username: this.state.username
        };
        this.props.updateProfile(user);
        this.setState({
            editingUsername: false,
            username: "",
            editingAvatar: false,
            avatar: "",
            canvasToDelete: null
        });
    };

    cancelUsername = () => {
        this.setState({
            editingUsername: false,
            username: "",
            editingAvatar: false,
            avatar: "",
            canvasToDelete: null
        });
    };

    editAvatar = (e) => {
        e.preventDefault();
        this.setState({
            editingUsername: false,
            username: "",
            editingAvatar: true,
            avatar: this.props.user.avatar,
            canvasToDelete: null
        });
    };

    changeAvatar = (avatar: string) => {
        this.setState({ avatar });
    };

    submitAvatar = () => {
        const user = {
            ...this.props.user,
            avatar: this.state.avatar
        };
        this.props.updateProfile(user);
        this.setState({
            editingUsername: false,
            username: "",
            editingAvatar: false,
            avatar: "",
            canvasToDelete: null
        });
    };

    cancelAvatar = () => {
        this.setState({
            editingUsername: false,
            username: "",
            editingAvatar: false,
            avatar: "",
            canvasToDelete: null
        });
    };

    deleteCanvas = (canvas: CanvasRecord<string>) => {
        this.setState({ canvasToDelete: canvas });
    };

    cancelDeleteCanvas = () => {
        this.setState({ canvasToDelete: null });
    };

    confirmDeleteCanvas = () => {
        this.props.deleteCanvas(this.state.canvasToDelete);
        this.setState({ canvasToDelete: null });
    };

    render() {
        if (this.props.loading || !this.props.user) {
            return <Loading />;
        }
        return (
            <Page>
                <Header />
                <PageContent>
                    <div className="profile-page">
                        <div className="profile-page__header">
                            <div className="profile-page__header-inner">
                                <div className="profile-page__avatar">
                                    <Avatar user={this.props.user} height="120px" width="120px" />
                                    {this.props.user.id === this.props.self.id && (
                                        <div className="profile-page__avatar-overlay" onClick={this.editAvatar}>
                                            <Icon name="pencil" />
                                        </div>
                                    )}
                                </div>
                                {this.state.editingAvatar ? (
                                    <div className="profile-page__edit-avatar">
                                        <label>Choose an image URL:</label>
                                        <Input
                                            name="avatar"
                                            value={this.state.avatar}
                                            onChange={this.changeAvatar}
                                        />
                                        <Button onClick={this.submitAvatar}>
                                            <Icon name="check" />
                                        </Button>
                                        <Button warning={true} onClick={this.cancelAvatar}>
                                            <Icon name="times" />
                                        </Button>
                                    </div>
                                ) : (
                                    <div className="profile-page__header-details">
                                        {this.state.editingUsername ? (
                                            <div className="profile-page__edit-username">
                                                <Input
                                                    name="username"
                                                    value={this.state.username}
                                                    onChange={this.changeUsername}
                                                />
                                                <Button onClick={this.submitUsername}>
                                                    <Icon name="check" />
                                                </Button>
                                                <Button warning={true} onClick={this.cancelUsername}>
                                                    <Icon name="times" />
                                                </Button>
                                            </div>
                                        ) : (
                                            <h2>
                                                {this.props.user.username}
                                                {this.props.user.id === this.props.self.id && (
                                                    <a href="#" onClick={this.editUsername}>
                                                        <Icon name="pencil" />
                                                    </a>
                                                )}
                                            </h2>
                                        )}
                                        <h3>Last login {distanceInWords(this.props.user.last_login, new Date())} ago</h3>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="profile-page__canvas-list">
                            {this.props.canvases.length > 0 && (
                                <CanvasList
                                    canvases={this.props.canvases}
                                    user={this.props.self}
                                    loadMore={()=>{}}
                                    joinCanvas={this.props.joinCanvas}
                                    deleteCanvas={this.deleteCanvas}
                                />
                            )}
                        </div>
                        <Modal
                            title={this.state.canvasToDelete ? this.state.canvasToDelete.title : ""}
                            open={!!this.state.canvasToDelete}
                            onClose={this.cancelDeleteCanvas}
                            height={175}
                        >
                            <p>Are you sure you want to delete this canvas?</p>
                            <Button warning={true} onClick={this.confirmDeleteCanvas}>Confirm</Button>
                            <Button onClick={this.cancelDeleteCanvas}>Cancel</Button>
                        </Modal>
                    </div>
                </PageContent>
            </Page>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const isUser = !!state.profile.user && state.profile.user.id === parseInt(ownProps.params.id, 10);
    return {
        self: state.auth.user,
        loading: state.profile.loading,
        user: isUser ? state.profile.user : null,
        canvases: isUser ? state.profile.canvases : []
    };
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    loadProfile() {
        dispatch(loadProfile(parseInt(ownProps.params.id, 10)));
    },
    updateProfile(user: UserRecord) {
        dispatch(updateProfile(user));
    },
    joinCanvas(slug: string) {
        dispatch(push(`/canvas/${slug}`));
    },
    deleteCanvas(canvas: CanvasRecord<string>) {
        dispatch(deleteCanvas(canvas));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);