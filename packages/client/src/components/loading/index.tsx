import React from "react";
import Icon from "../icon";
import Spinner from "../spinner";
import "./style.scss";

export default function Loading() {
    return <div className="loading"><Spinner /></div>
}