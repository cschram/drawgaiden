import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { UserRecord } from "@drawgaiden/common";
import { logout } from "../../actions/auth";
import Avatar from "../avatar";
import { Dropdown, DropdownItem } from "../dropdown";
import Icon from "../icon";
import Input from "../input";
import logoSrc from "../../img/logo.png";
import "./style.scss";

interface HeaderProps {
    user: UserRecord;
    logout(): void;
}

interface HeaderState {
    userMenuOpen: boolean;
}

class Header extends React.Component<HeaderProps, HeaderState> {
    constructor(props) {
        super(props);
        this.state = {
            userMenuOpen: false
        };
    }

    private toggleUserMenu = (e) => {
        e.stopPropagation();
        this.setState({ userMenuOpen: !this.state.userMenuOpen });
    };

    private closeUserMenu = (e) => {
        this.setState({ userMenuOpen: false });
    };

    render() {
        return (
            <div className="header" onClick={this.closeUserMenu}>
                <div className="header__inner">
                    <div className="header__logo">
                        <Link to="/">
                            <img
                                src={logoSrc}
                                title="Draw Gaiden"
                            />
                        </Link>
                    </div>
                    <div className="header__content">{this.props.children}</div>
                    {this.props.user && (
                        <div className="header__user-menu">
                            <div className="header__user-menu__handle" onClick={this.toggleUserMenu}>
                                <Avatar user={this.props.user} />
                                <Icon
                                    className="header__user-menu__icon"
                                    name={this.state.userMenuOpen ? "angle-up" : "angle-down"}
                                />
                            </div>
                            <Dropdown open={this.state.userMenuOpen}>
                                <DropdownItem>Logged in as {this.props.user.username}</DropdownItem>
                                <DropdownItem><hr /></DropdownItem>
                                <DropdownItem>
                                    <Link to="/">
                                        <Icon name="home" />
                                        Home
                                    </Link>
                                </DropdownItem>
                                <DropdownItem>
                                    <Link to={`/profile/${this.props.user.id}`}>
                                        <Icon name="user" />
                                        Profile
                                    </Link>
                                </DropdownItem>
                                <DropdownItem>
                                    <Link to="/canvas/create">
                                        <Icon name="plus" />
                                        Create a Canvas
                                    </Link>
                                </DropdownItem>
                                <DropdownItem>
                                    <a href="https://gitlab.com/cschram/drawgaiden/issues" target="_blank">
                                        <Icon name="external-link" />
                                    Issue Tracker
                                    </a>
                                </DropdownItem>
                                <DropdownItem onClick={this.props.logout}>
                                    <Icon name="sign-out" />
                                    Logout
                                </DropdownItem>
                            </Dropdown>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    user: state.auth.user,
    children: ownProps.children
});

const mapDispatchToProps = (dispatch) => ({
    logout() {
        dispatch(logout());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);