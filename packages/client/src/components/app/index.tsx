import React from "react";
import { connect } from "react-redux";
import { connect as socketConnect } from "../../actions/connection";
import Loading from "../loading";
import "./style.scss";

interface AppProps {
    connected: boolean;
    connecting: boolean;
    connect: () => void;
}

class App extends React.Component<AppProps> {
    componentDidMount() {
        this.checkConnection();
    }

    componentDidUpdate() {
        this.checkConnection();
    }

    private checkConnection() {
        if (!this.props.connected && !this.props.connecting) {
            this.props.connect();
        }
    }

    render() {
        return (
            <div className="app">
                {this.props.connected ? this.props.children : <Loading />}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    connected: state.connection.connected,
    connecting: state.connection.connecting
});

const mapDispatchToProps = (dispatch) => ({
    connect() {
        dispatch(socketConnect());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);