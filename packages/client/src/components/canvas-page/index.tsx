import * as React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import shallowCompare from "react-addons-shallow-compare";
import throttle from "lodash.throttle";
import {
    CanvasRecord,
    HistoryData,
    HistoryRecord,
    Coord,
    UserRecord
} from "@drawgaiden/common";
import Easel from "@drawgaiden/easel";
import BrushTool from "@drawgaiden/easel/lib/tools/brush";
import EaselContainer from "./easel-container";
import ShareModal from "./share-modal";
import Modal from "../modal";
import Icon from "../icon";
import Loading from "../loading";
import {
    joinCanvas,
    leaveCanvas,
    draw,
    setMousePosition,
    changeTool,
    changeLayer,
    changeStrokeColor,
    changeFillColor,
    changeToolSetting
} from "../../actions/canvas";
import "./style.scss";

const tools = [
    { value: "pencil", label: <Icon name="pencil" /> },
    { value: "brush", label: <Icon name="paint-brush" /> },
    { value: "rectangle", label: <Icon name="square-o" /> },
    { value: "circle", label: <Icon name="circle-o" /> },
    { value: "eraser", label: <Icon name="eraser" /> },
    { value: "move", label: <Icon name="arrows-alt" /> },
    { value: "colorpicker", label: <Icon name="eyedropper" /> }
];

const brushes = {
    "airbrush": require("../../img/brushes/airbrush.png"),
    "chalk": require("../../img/brushes/chalk.png"),
    "charcoal": require("../../img/brushes/charcoal.png"),
    "paintbrush_flat_rough": require("../../img/brushes/paintbrush_flat_rough.png"),
    "paintbrush_flat_soft": require("../../img/brushes/paintbrush_flat_soft.png"),
    "paintbrush_smooth": require("../../img/brushes/paintbrush_smooth.png"),
    "paintbrush_standard": require("../../img/brushes/paintbrush_standard.png"),
    "pencil_rough": require("../../img/brushes/pencil_rough.png"),
    "pencil_soft": require("../../img/brushes/pencil_soft.png")
};

const brushOptions = [
    { value: "airbrush", label: "Air Brush" },
    { value: "chalk", label: "Chalk" },
    { value: "charcoal", label: "Charcoal" },
    { value: "paintbrush_flat_rough", label: "Flat Rough Paintbrush" },
    { value: "paintbrush_flat_soft", label: "Flat Soft Paintbrush" },
    { value: "paintbrush_smooth", label: "Smooth Paintbrush" },
    { value: "paintbrush_standard", label: "Standard Paintbrush" },
    { value: "pencil_soft", label: "Soft Pencil" },
    { value: "pencil_rough", label: "Rough Pencil" }
];

interface CanvasPageProps {
    user: UserRecord;
    loading: boolean;
    slug: string;
    canvas: CanvasRecord<string>;
    history: Array<HistoryRecord>;
    latestEntry: HistoryRecord;
    joinCanvas(id: string): void;
    exit(): void;
    draw(entry: HistoryData): void;
    setMousePosition(coord: Coord): void;
    currentTool: string;
    changeTool(tool: string): void;
    currentLayer: number;
    changeLayer(layer: number): void;
    strokeColor: string;
    changeStrokeColor(color: string): void;
    fillColor: string;
    changeFillColor(color: string): void;
    toolSize: number;
    changeToolSize(size: number): void;
    toolOpacity: number;
    changeToolOpacity(opacity: number): void;
    toolSmoothLines: boolean;
    changeToolSmoothLines(smoothLines: boolean): void;
    toolBrush: string;
    changeToolBrush(brush: string): void;
}

interface CanvasPageState {
    hideControls: boolean;
    shareModalOpen: boolean;
}

export class CanvasPage extends React.Component<CanvasPageProps, CanvasPageState> {
    easel: Easel;
    queue: Array<HistoryRecord>;

    constructor(props) {
        super(props);
        this.state = {
            hideControls: false,
            shareModalOpen: false
        };
    }

    componentDidMount() {
        this.loadCanvas();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.latestEntry !== this.props.latestEntry) {
            this.queue.push(nextProps.latestEntry);
            this.drainQueue();
        }
        return shallowCompare(this, nextProps, nextState);
    }

    componentDidUpdate() {
        this.loadCanvas();
        this.updateEasel();
    }

    private loadCanvas() {
        if (!this.props.loading &&
            (this.props.canvas == null || this.props.canvas.slug !== this.props.slug)
        ) {
            this.props.joinCanvas(this.props.slug);
        }
    }

    private initEasel = async (container) => {
        if (!this.easel && container) {
            this.queue = [];
            const brushPromises = Object.keys(brushes).map(brush => {
                return new Promise((resolve, reject) => {
                    const image = new Image();
                    image.src = brushes[brush];
                    image.onload = resolve;
                    image.onerror = reject;
                    BrushTool.brushes[brush] = image;
                });
            });
            await Promise.all(brushPromises);
            this.easel = new Easel(container, {
                width: this.props.canvas.width,
                height: this.props.canvas.height,
                backgroundColor: this.props.canvas.background_color,
                layers: this.props.canvas.layers,
                onMouseMove: throttle(this.props.setMousePosition, 33),
                onDraw: this.onDraw,
                onPick: this.onPick
            });
            this.updateEasel();
            if (this.props.canvas.snapshots && this.props.canvas.snapshots.length > 0) {
                const snapshotPromises = this.props.canvas.snapshots.map((snapshot, i) => {
                    return new Promise((resolve, reject) => {
                        const img = new Image();
                        img.onload = () => {
                            this.easel.drawImage(img, { x: 0, y: 0 }, i);
                            resolve();
                        };
                        img.onerror = () => {
                            reject();
                        };
                        img.src = snapshot;
                    });
                });
                await Promise.all(snapshotPromises);
                this.drainQueue();
            } else {
                this.drainQueue();
            }
        }
    };

    private updateEasel() {
        if (this.easel) {
            this.easel.setTool(this.props.currentTool);
            this.easel.setLayer(this.props.currentLayer);
            this.easel.setStrokeColor(this.props.strokeColor);
            this.easel.setFillColor(this.props.fillColor);
            this.easel.setToolSize(this.props.toolSize, this.props.currentTool);
            this.easel.setToolOpacity(this.props.toolOpacity, this.props.currentTool);
            this.easel.setToolSmoothLines(this.props.toolSmoothLines, this.props.currentTool);
            this.easel.setToolBrush(this.props.toolBrush, this.props.currentTool);
        }
    }

    /**
     * Drain queue of history entries, calling easel.draw for each one.
     * A queue is used to request an animation frame for each entry in order
     * to prevent locking up the UI thread.
     */
    private drainQueue() {
        if (this.queue.length > 0 && this.easel) {
            let entry = this.queue.shift();
            requestAnimationFrame(async () => {
                await this.easel.draw(entry.tool_name, entry.path, entry.settings);
                this.drainQueue();
            });
        }
    }

    private onDraw = (path: Coord[]) => {
        let entry: HistoryData = {
            canvas_id: this.props.canvas.id,
            user_id: this.props.user.id,
            tool_name: this.easel.getTool(),
            settings: this.easel.getToolSettings(),
            path
        };
        this.props.draw(entry);
    };

    private onPick = (type: string, color: string, opacity: number) => {
        if (type === "stroke") {
            this.props.changeStrokeColor(color);
        } else {
            this.props.changeFillColor(color);
        }
        this.props.changeToolOpacity(opacity);
    };

    private onToggleControls = () => {
        this.setState({
            hideControls: !this.state.hideControls,
            shareModalOpen: false
        });
    };

    private onShare = () => {
        this.setState({
            hideControls: false,
            shareModalOpen: true
        });
    };

    private onSave = () => {
        window.open(
            `${window.location.origin}/api/canvas/${this.props.canvas.slug}/screenshot.png`,
            "_blank"
        );
    };

    private onExit = () => {
        this.props.exit();
    };

    private closeModal = () => {
        this.setState({
            hideControls: false,
            shareModalOpen: false
        });
    };

    render() {
        if (this.props.canvas == null || this.props.canvas.slug !== this.props.slug) {
            return <Loading />;
        }
        return (
            <div className="canvas-page">
                <EaselContainer
                    innerRef={this.initEasel}
                    hideControls={this.state.hideControls}
                    toggleControls={this.onToggleControls}
                    tools={tools}
                    currentTool={this.props.currentTool}
                    onChangeTool={this.props.changeTool}
                    currentLayer={this.props.currentLayer}
                    numLayers={this.props.canvas.layers}
                    onChangeLayer={this.props.changeLayer}
                    onShare={this.onShare}
                    onSave={this.onSave}
                    onExit={this.onExit}
                    strokeColor={this.props.strokeColor}
                    onChangeStrokeColor={this.props.changeStrokeColor}
                    fillColor={this.props.fillColor}
                    onChangeFillColor={this.props.changeFillColor}
                    toolSize={this.props.toolSize}
                    onChangeToolSize={this.props.changeToolSize}
                    toolOpacity={this.props.toolOpacity}
                    onChangeToolOpacity={this.props.changeToolOpacity}
                    toolSmoothLines={this.props.toolSmoothLines}
                    onChangeToolSmoothLines={this.props.changeToolSmoothLines}
                    brushes={brushOptions}
                    toolBrush={this.props.toolBrush}
                    onChangeToolBrush={this.props.changeToolBrush} />
                <Modal title={"Share this canvas"}
                       open={this.state.shareModalOpen}
                       height={150}
                       width={300}
                       onClose={this.closeModal}>
                    <ShareModal />
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const toolSettings = state.canvas.toolSettings[state.canvas.currentTool] || {
        size: 1,
        opacity: 100,
        smoothness: 80
    };
    return {
        user: state.auth.user,
        loading: state.canvas.loading,
        slug: ownProps.params.id,
        canvas: state.canvas.canvas,
        history: state.canvas.history,
        latestEntry: state.canvas.latestEntry,
        currentTool: state.canvas.currentTool,
        currentLayer: state.canvas.currentLayer,
        strokeColor: state.canvas.strokeColor,
        fillColor: state.canvas.fillColor,
        toolSize: toolSettings.size,
        toolOpacity: toolSettings.opacity,
        toolSmoothLines: toolSettings.smoothLines,
        toolBrush: toolSettings.brush
    };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    joinCanvas(slug: string) {
        dispatch(joinCanvas(slug));
    },
    exit() {
        dispatch(leaveCanvas());
        dispatch(push("/"));
    },
    draw(entry: HistoryRecord) {
        dispatch(draw(entry));
    },
    setMousePosition(coord: Coord) {
        dispatch(setMousePosition(coord));
    },
    changeTool(tool: string) {
        dispatch(changeTool(tool));
    },
    changeLayer(layer: number) {
        dispatch(changeLayer(layer));
    },
    changeStrokeColor(color: string) {
        dispatch(changeStrokeColor(color));
    },
    changeFillColor(color: string) {
        dispatch(changeFillColor(color));
    },
    changeToolSize(size: number) {
        dispatch(changeToolSetting("size", size));
    },
    changeToolOpacity(opacity: number) {
        dispatch(changeToolSetting("opacity", opacity));
    },
    changeToolSmoothLines(smoothLines: boolean) {
        dispatch(changeToolSetting("smoothLines", smoothLines));
    },
    changeToolBrush(brush: string) {
        dispatch(changeToolSetting("brush", brush));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CanvasPage);