import * as React from "react";
import { UserRecord } from "@drawgaiden/common";
import Button from "../button";
import Icon from "../icon";
import Checkbox from "../checkbox";
import ColorPicker from "../color-picker";
import { default as RadioButton, RadioOption } from "../radio-button";
import Select, { SelectOption } from "../select";
import Slider from "../slider";

const settingsMap = {
    brush: ["color", "size", "opacity", "brush"],
    circle: ["color", "size", "opacity"],
    colorpicker: ["color"],
    eraser: ["size", "opacity", "smooth-lines"],
    pencil: ["color", "size", "opacity", "smooth-lines"],
    rectangle: ["color", "size", "opacity"]
};

function hasSetting(tool: string, setting: string): boolean {
    return settingsMap[tool] && settingsMap[tool].indexOf(setting) > -1;
}

interface EaselContainerProps {
    innerRef(el: HTMLElement): void;
    hideControls: boolean;
    toggleControls(): void;
    tools: RadioOption[];
    currentTool: string;
    onChangeTool(tool: string): void;
    currentLayer: number;
    numLayers: number;
    onChangeLayer(layer: number): void;
    onShare(): void;
    onSave(): void;
    onExit(): void;
    strokeColor: string;
    onChangeStrokeColor(color: string): void;
    fillColor: string;
    onChangeFillColor(color: string): void;
    toolSize: number;
    onChangeToolSize(size: number): void;
    toolOpacity: number;
    onChangeToolOpacity(opacity: number): void;
    toolSmoothLines: boolean;
    onChangeToolSmoothLines(smoothLines: boolean): void;
    brushes: SelectOption[];
    toolBrush: string;
    onChangeToolBrush(brush: string): void;
}

export default function EaselContainer({
    innerRef,
    hideControls,
    toggleControls,
    tools,
    currentTool,
    onChangeTool,
    currentLayer,
    onChangeLayer,
    numLayers,
    onShare,
    onSave,
    onExit,
    strokeColor,
    onChangeStrokeColor,
    fillColor,
    onChangeFillColor,
    toolSize,
    onChangeToolSize,
    toolOpacity,
    onChangeToolOpacity,
    toolSmoothLines,
    onChangeToolSmoothLines,
    brushes,
    toolBrush,
    onChangeToolBrush
}: EaselContainerProps) {
    const layerOptions = [];
    for (let i = 0; i < numLayers; i++) {
        layerOptions.push({
            value: i.toString(),
            label: `Layer ${i + 1}`
        });
    }
    const _onChangeLayer = (value: string) => {
        onChangeLayer(parseInt(value, 10));
    };
    return (
        <div className="canvas-page__easel-container">
            <div className="canvas-page__easel" ref={innerRef}></div>
            <div className="canvas-page__controls">
                <Button transparent={hideControls}
                        className="canvas-page__control-toggle"
                        onClick={toggleControls}>
                    <Icon name={hideControls ? "eye-slash" : "eye"} />
                </Button>
                {hideControls ? null :
                    <span>
                        <RadioButton name="tools"
                                     value={currentTool}
                                     options={tools}
                                     className="canvas-page__control"
                                     onChange={onChangeTool} />
                        <Select value={currentLayer.toString()}
                                options={layerOptions}
                                className="canvas-page__control"
                                onChange={_onChangeLayer} />
                        <Button className="canvas-page__control" onClick={onShare}>
                            <Icon name="share-alt" />
                            <span>Share</span>
                        </Button>
                        <Button className="canvas-page__control" onClick={onSave}>
                            <Icon name="floppy-o" />
                            <span>Save</span>
                        </Button>
                        <Button className="canvas-page__control" onClick={onExit}>
                            <Icon name="times" />
                            <span>Exit</span>
                        </Button>
                    </span>
                }
            </div>
            {hideControls ? null :
                <div className="canvas-page__settings">
                    {hasSetting(currentTool, "color") && (
                        <div className="canvas-page__tool-setting canvas-page__colors">
                            <ColorPicker color={strokeColor}
                                        onChange={onChangeStrokeColor} />
                            <ColorPicker color={fillColor}
                                        onChange={onChangeFillColor} />
                        </div>
                    )}
                    {hasSetting(currentTool, "size") && (
                        <div className="canvas-page__tool-setting">
                            <label>Size</label>
                            <Slider name="size"
                                    value={toolSize}
                                    min={1}
                                    max={50}
                                    onChange={onChangeToolSize} />
                        </div>
                    )}
                    {hasSetting(currentTool, "opacity") && (
                        <div className="canvas-page__tool-setting">
                            <label>Opacity</label>
                            <Slider name="opacity"
                                    value={toolOpacity}
                                    min={0}
                                    max={100}
                                    onChange={onChangeToolOpacity} />
                        </div>
                    )}
                    {hasSetting(currentTool, "brush") && (
                        <Select
                            className="canvas-page__tool-setting"
                            value={toolBrush}
                            options={brushes}
                            onChange={onChangeToolBrush}
                        />
                    )}
                    {hasSetting(currentTool, "smooth-lines") && (
                        <div className="canvas-page__tool-setting">
                            <Checkbox
                                name="smooth-lines"
                                checked={toolSmoothLines}
                                onChange={onChangeToolSmoothLines}
                            >
                                Smooth Lines
                            </Checkbox> 
                        </div>
                    )}
                </div>
            }
        </div>
    );
}