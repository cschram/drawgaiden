import React from "react";
import Header from "../header";
import { Page, PageContent } from "../page";
import "./style.scss";

export default function NotFoundPage() {
    return (
        <Page>
            <Header />
            <PageContent>
                <div className="not-found-page">
                    <h1>Sorry, there's nothing here!</h1>
                </div>
            </PageContent>
        </Page>
    );
}