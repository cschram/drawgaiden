import * as React from "react";
import Loading from "../loading";
import "./style.scss";

interface LazyImageProps {
    src: string;
    title: string;
    width: number;
    height: number;
}

interface LazyImageState {
    loaded: boolean;
}

export default class LazyImage extends React.Component<LazyImageProps, LazyImageState> {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        };
    }

    private image: HTMLImageElement;

    onRef = (image: HTMLImageElement) => {
        this.image = image;
        if (this.image) {
            this.image.onload = this.onLoad;
            this.image.src = this.props.src;
        }
    };

    onLoad = () => {
        if (this.image) {
            this.setState({ loaded: true });
        }
    };

    render() {
        const { src, title, width, height } = this.props;
        const { loaded } = this.state;
        const style = {
            width: `${width}px`,
            height: `${height}px`
        };
        return (
            <div className={`lazy-image ${loaded ? "lazy-image--loaded" : ""}`} style={style}>
                <img ref={this.onRef} src={src} title={title} />
                {!loaded && <Loading />}
            </div>
        );
    }
}