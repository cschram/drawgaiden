import * as React from "react";
import Button from "../button";
import Checkbox from "../checkbox";
import ColorPicker from "../color-picker";
import Input from "../input";
import Select from "../select";
import Slider from "../slider";
import Spinner from "../spinner";

interface CreateCanvasFormProps {
    title: string;
    onChangeTitle(title: string): void;
    is_public: boolean;
    onChangeis_public(is_public: boolean): void;
    width: number;
    onChangeWidth(width: number): void;
    height: number;
    onChangeHeight(height: number): void;
    layers: string;
    onChangeLayers(layers: string): void;
    background_color: string;
    onChangebackground_color(color: string): void;
    submit(): void;
    creating: boolean;
    error: string;
}

export default function CreateCanvasForm({
    title,
    onChangeTitle,
    is_public,
    onChangeis_public,
    width,
    onChangeWidth,
    height,
    onChangeHeight,
    layers,
    onChangeLayers,
    background_color,
    onChangebackground_color,
    submit,
    creating,
    error
}: CreateCanvasFormProps) {
    const layerOptions = [];
    for (let i = parseInt(process.env.CANVAS_MIN_LAYERS, 10); i <= parseInt(process.env.CANVAS_MAX_LAYERS, 10); i++) {
        layerOptions.push({
            value: i.toString(),
            label: i.toString()
        });
    }
    return (
        <form className="create-canvas-form">
            <h2>Canvas Options</h2>
            <div className="create-canvas-form__field">
                <label>Title</label>
                <Input
                    name="canvas-title"
                    placeholder="Title text"
                    value={title}
                    onChange={onChangeTitle}
                />
            </div>
            <div className="create-canvas-form__field">
                <label>Width</label>
                <div>{width}</div>
                <Slider
                    name="canvas-width"
                    min={parseInt(process.env.CANVAS_MIN_WIDTH, 10)}
                    max={parseInt(process.env.CANVAS_MAX_WIDTH, 10)}
                    value={width}
                    onChange={onChangeWidth}
                />
            </div>
            <div className="create-canvas-form__field">
                <label>Height</label>
                <div>{height}</div>
                <Slider
                    name="canvas-height"
                    min={parseInt(process.env.CANVAS_MIN_HEIGHT, 10)}
                    max={parseInt(process.env.CANVAS_MAX_HEIGHT, 10)}
                    value={height}
                    onChange={onChangeHeight}
                />
            </div>
            <div className="create-canvas-form__field">
                <label>Number of Layers</label>
                <Select
                    value={layers}
                    options={layerOptions}
                    onChange={onChangeLayers}
                />
            </div>
            <div className="create-canvas-form__field">
                <label>Background Color</label>
                <ColorPicker
                    color={background_color}
                    onChange={onChangebackground_color}
                />
            </div>
            <div className="create-canvas-form__field">
                <Checkbox
                    name="canvas-public"
                    checked={is_public}
                    onChange={onChangeis_public}
                >
                    Public
                </Checkbox>
            </div>
            {error && <div className="create-canvas-form__error">{error}</div>}
            <Button onClick={submit}>{creating ? <Spinner /> : "Create Canvas"}</Button>
        </form>
    );
}