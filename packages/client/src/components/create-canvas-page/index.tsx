import * as React from "react";
import { connect } from "react-redux";
import { CanvasOptions } from "@drawgaiden/common";
import { createCanvas } from "../../actions/canvas";
import Header from "../header";
import { Page, PageContent } from "../page";
import CreateCanvasForm from "./create-canvas-form";
import "./style.scss";

interface CreateCanvasPageProps {
    username: string;
    submit(options: CanvasOptions): void;
    creating: boolean;
    error: string;
}

interface CreateCanvasPageState {
    title: string;
    is_public: boolean;
    width: number;
    height: number;
    layers: string;
    background_color: string;
}

class CreateCanvasPage extends React.Component<CreateCanvasPageProps, CreateCanvasPageState> {
    constructor(props) {
        super(props);
        this.state = {
            title: `${props.username}'s canvas`,
            is_public: true,
            width: parseInt(process.env.CANVAS_DEFAULT_WIDTH, 10),
            height: parseInt(process.env.CANVAS_DEFAULT_HEIGHT, 10),
            layers: process.env.CANVAS_DEFAULT_LAYERS,
            background_color: "#ffffff"
        };
    }

    onChangeTitle = (title: string) => {
        this.setState({ title });
    };

    onChangeis_public = (is_public: boolean) => {
        this.setState({ is_public });
    };

    onChangeWidth = (width: number) => {
        this.setState({ width });
    };

    onChangeHeight = (height: number) => {
        this.setState({ height });
    };

    onChangeLayers = (layers: string) => {
        this.setState({ layers });
    };

    onChangebackground_color = (background_color: string) => {
        this.setState({ background_color });
    };

    submit = () => {
        const options: CanvasOptions = {
            title: this.state.title,
            is_public: this.state.is_public,
            width: this.state.width,
            height: this.state.height,
            layers: parseInt(this.state.layers, 10),
            background_color: this.state.background_color
        };
        this.props.submit(options);
    };

    render() {
        return (
            <Page>
                <Header />
                <PageContent>
                    <div className="create-canvas-page">
                        <CreateCanvasForm
                            title={this.state.title}
                            onChangeTitle={this.onChangeTitle}
                            is_public={this.state.is_public}
                            onChangeis_public={this.onChangeis_public}
                            width={this.state.width}
                            onChangeWidth={this.onChangeWidth}
                            height={this.state.height}
                            onChangeHeight={this.onChangeHeight}
                            layers={this.state.layers}
                            onChangeLayers={this.onChangeLayers}
                            background_color={this.state.background_color}
                            onChangebackground_color={this.onChangebackground_color}
                            submit={this.submit}
                            creating={this.props.creating}
                            error={this.props.error}
                        />
                    </div>
                </PageContent>
            </Page>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    username: state.auth.user.username,
    creating: state.canvas.creatingCanvas,
    error: state.canvas.creatingCanvasError
});

const mapDispatchToprops = (dispatch, ownProps) => ({
    submit(options: CanvasOptions) {
        dispatch(createCanvas(options));
    }
});

export default connect(mapStateToProps, mapDispatchToprops)(CreateCanvasPage);