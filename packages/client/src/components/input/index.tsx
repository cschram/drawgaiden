import * as React from "react";
import "./style.scss";

interface InputProps {
    name: string;
    value: string;
    onChange: (value: string) => void;
    disabled?: boolean;
    placeholder?: string;
    className?: string;
    innerRef?: (el: HTMLInputElement) => void;
    onClick?: () => void;
}

interface InputState {
    value: string;
}

class Input extends React.Component<InputProps, InputState> {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
    }

    private onChange = (e) => {
        e.preventDefault();
        if (!this.props.disabled) {
            this.setState({ value: e.target.value });
            this.props.onChange(e.target.value);
        }
    };

    render() {
        let {
            name,
            disabled = false,
            placeholder = "",
            className = "",
            innerRef = () => {},
            onClick = () => {}
        } = this.props;
        let { value } = this.state;
        className = `input ${className}`;
        if (disabled) {
            className = `${className} input--disabled`;
        }
        return <input className={className}
                      ref={innerRef}
                      type="text"
                      name={name}
                      value={value}
                      disabled={disabled}
                      placeholder={placeholder}
                      onChange={this.onChange}
                      onClick={onClick} />;
    }
}

export default Input;