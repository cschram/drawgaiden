import * as React from "react";
import { Link } from "react-router";
import { UserRecord, CanvasRecord } from "@drawgaiden/common";
import Button from "../button";
import LazyImage from "../lazy-image";
import "./style.scss";

const thumbnailSize = {
    width: 150,
    height: 150
};

interface CanvasListProps {
    user: UserRecord;
    canvases: Array<CanvasRecord<string>>;
    loadMore(): void;
    joinCanvas(slug: string): void;
    deleteCanvas(canvas: CanvasRecord<string>): void;
}

export default function CanvasList({
    user,
    canvases,
    loadMore,
    joinCanvas,
    deleteCanvas
}: CanvasListProps) {
    return (
        <ul className="canvas-list">
            {canvases.length === 0 && (
                <div className="canvas-list__empty">
                    There's nothing here! Why not <Link to="/canvas/create">create a canvas</Link>?
                </div>
            )}
            {canvases.map((canvas, i) => {
                const onJoin = () => {
                    joinCanvas(canvas.slug);
                };
                const onDelete = () => {
                    deleteCanvas(canvas);
                };
                return (
                    <li key={i} className="canvas-list__item">
                        <div className="canvas-list__item-thumbnail">
                            <LazyImage
                                src={`/api/canvas/${canvas.slug}/thumbnail.png?width=${thumbnailSize.width}&height=${thumbnailSize.height}`}
                                title={canvas.title}
                                width={thumbnailSize.width}
                                height={thumbnailSize.height}
                            />
                        </div>
                        <div className="canvas-list__item-details">
                            <h2>{canvas.title}</h2>
                            <h3>Created by <Link to={`/profile/${canvas.owner_id}`}>{canvas.owner_username}</Link></h3>
                            <div className="canvas-list__item-controls">
                                <Button onClick={onJoin}>Join</Button>
                                {user.id === canvas.owner_id && (
                                    <Button warning={true} onClick={onDelete}>Delete</Button>
                                )}
                            </div>
                        </div>
                    </li>
                );
            })}
        </ul>
    );
}