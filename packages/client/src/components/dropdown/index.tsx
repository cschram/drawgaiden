import * as React from "react";
import "./style.scss";

export function Dropdown({ open = false, children }) {
    if (!open) {
        return null;
    }
    return (
        <ul className="dropdown">
            {children}
        </ul>
    );
}

export function DropdownItem({ onClick = ()=>{}, children }) {
    return <li className="dropdown-item" onClick={onClick}>{children}</li>;
}