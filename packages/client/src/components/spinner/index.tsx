import React from "react";
import Icon from "../icon";
import "./style.scss";

export default function Spinner() {
    return <Icon name="spinner" className="spinner" />;
}