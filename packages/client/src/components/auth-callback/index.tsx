import * as React from "react";
import { handleCallback } from "../../actions/auth";
import Loading from "../loading";
import { connect } from "react-redux";

interface AuthCallbackProps {
    callback(): void;
}

class AuthCallbackPage extends React.Component<AuthCallbackProps> {
    componentDidMount() {
        this.props.callback();
    }

    render() {
        return <Loading />;
    }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    callback: () => {
        dispatch(handleCallback());
    }
});

export default connect(() => ({}), mapDispatchToProps)(AuthCallbackPage);