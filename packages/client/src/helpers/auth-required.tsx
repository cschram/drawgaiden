import React from "react";
import { connect } from "react-redux";
import { login } from "../actions/auth";
import { connect as socketConnect } from "../actions/connection";
import Loading from "../components/loading";

function authRequired(component: React.ComponentClass | React.StatelessComponent): React.ComponentClass {
    interface AuthRequiredProps {
        expiresAt: number;
        ownProps: any;
        login: () => void;
    }
    class AuthRequired extends React.Component<AuthRequiredProps> {
        private isAuthenticated() {
            return Date.now() < this.props.expiresAt;
        }
        private checkLogin() {
            if (!this.isAuthenticated()) {
                this.props.login();
            }
        }
        componentDidMount() {
            this.checkLogin();
        }
        componentDidUpdate() {
            this.checkLogin();
        }
        render() {
            if (!this.isAuthenticated()) {
                return <Loading />;
            }
            return React.createElement(component, this.props.ownProps);
        }
    }
    const mapStateToProps = (state, ownProps) => ({
        expiresAt: state.auth.expiresAt,
        ownProps
    });
    const mapDispatchToProps = (dispatch, ownProps) => ({
        login: () => {
            dispatch(login(window.location.pathname));
        }
    });
    return connect(mapStateToProps, mapDispatchToProps)(AuthRequired);
}

export default authRequired;