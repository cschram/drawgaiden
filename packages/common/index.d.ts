export = DrawGaiden;

declare namespace DrawGaiden {
    //#region Users
    export interface UserData {
        avatar: string;
        sub: string;
        username: string;
    }

    export interface UserRecord extends UserData {
        created_at: Date;
        id: number;
        last_login: Date;
    }
    //#endregion

    //#region Sessions
    export interface SessionData {
        auth_token: string;
        user_id: number;
    }

    export interface SessionRecord extends SessionData {
        created_at: Date;
        id: number;
    }
    //#endregion

    //#region Canvases
    export interface CanvasOptions {
        background_color: string;
        height: number;
        is_public: boolean;
        layers: number;
        title: string;
        width: number;
    }

    export interface CanvasData extends CanvasOptions {
        owner_id: number;
    }

    export interface CanvasRecord<S> extends CanvasData {
        created_at: Date;
        id: number;
        owner_username: string;
        slug: string;
        snapshots: Array<S>;
    }
    //#endregion

    //#region History
    export interface Coord {
        x: number;
        y: number;
    }

    export interface HistoryData {
        canvas_id: number;
        path: Array<Coord>;
        settings: {
            brush?: string;
            fillStyle?: string;
            globalCompositeOperation?: string;
            layer?: number;
            lineCap?: string;
            lineJoin?: string;
            lineWidth?: number;
            opacity?: number;
            primary?: boolean;
            sendUpdates?: boolean;
            smoothLines?: boolean;
            strokeStyle?: string;
        };
        tool_name: string;
        user_id: number;
    }

    export interface HistoryRecord extends HistoryData {
        created_at: Date;
        id: number;
    }
    //#endregion
}